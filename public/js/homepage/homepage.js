$(document).ready(function() {
	socket_connect($('#curr_pairs').val(), $('#socket_server_url').val());
});

var socket;

function socket_connect(pairs,socket_url)
{
	socket = io.connect(socket_url);
	socket.on('notification', function (data) {
	    obj = JSON.parse(data);
	    update_spread(obj);
	});

	socket.on('which_room', function (data) {
		socket.emit('set_room',pairs);
	});

	return socket;
}

function update_spread(obj)
{	
	var pairName 	= Object.keys(obj);
	var objData 	= obj[pairName];
	
	var last2digt_ask = 0;
	var last2digt_bid = 0;
	
	var calculateChange = false;
	
//	console.log('pair: '+pairName);
	if('direction' in objData) {
		if(parseInt(objData.direction) == 1) {
			$('#spread_arrow_'+pairName).removeClass();
			$('#spread_arrow_'+pairName).addClass('arrow up');
		} else if (parseInt(objData.direction) == -1) {
			$('#spread_arrow_'+pairName).removeClass();
			$('#spread_arrow_'+pairName).addClass('arrow down');
		} else {
			$('#spread_arrow_'+pairName).removeClass();
			$('#spread_arrow_'+pairName).addClass('arrow no-change');
		}
	}
	
	
//	console.log(pairName.indexOf('JPY'));
	if(pairName.toString().indexOf('JPY') == -1) { // not exists
		var check_precision = 5;
	} else {
		var check_precision = 3;
	}
	
//	console.log(check_precision);
	
	if('ask' in objData) {
		var ask = objData.ask;
//		console.log('ask: '+ask);
//		console.log(decimalPlaces(ask));
		ask = addZeroes(ask, check_precision);
//		console.log('ask: '+ask);
//		console.log(decimalPlaces(ask));
		last2digt_ask = ask.slice(-2);
//		console.log('last2digt_ask: '+last2digt_ask);
		calculateChange = true;
		$('#spread_ask_' + pairName).html(ask);
		$('#spread_last2digt_ask_' + pairName).val(last2digt_ask);
	} 
	
	if('bid' in objData) {
		var bid = objData.bid;
//		console.log('bid: '+bid);
//		console.log(decimalPlaces(bid));
		bid = addZeroes(bid, check_precision);
//		console.log('bid: '+bid);
//		console.log(decimalPlaces(bid));
		var last2digt_bid = bid.slice(-2);
//		console.log('last2digt_bid: '+last2digt_bid);
		calculateChange = true;
		$('#spread_bid_' + pairName).html(last2digt_bid);
		$('#spread_last2digt_bid_' + pairName).val(last2digt_bid);
	} 
	
	if(calculateChange) {
		last2digt_ask = parseInt($('#spread_last2digt_ask_' + pairName).val());
		last2digt_bid = parseInt($('#spread_last2digt_bid_' + pairName).val());
		var change = Math.abs((last2digt_ask - last2digt_bid) / 10);
		$('#spread_change_' + pairName).html(change);
	}
	
//	console.log('/////////////////////////////////////////////////');
}


function decimalPlaces(num) 
{
	var match = (''+num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
	if (!match) { return 0; }
	  return Math.max(
	       0,
	       // Number of digits right of decimal point.
	       (match[1] ? match[1].length : 0)
	       // Adjust for scientific notation.
	       - (match[2] ? +match[2] : 0)
	  );
}

function addZeroes(num, decimal_places) 
{
	var num = Number(num);
	if (String(num).split(".").length < decimal_places || String(num).split(".")[1].length<= decimal_places ){
		num = num.toFixed(decimal_places);
	}
	return num;
}
