window.Parsley
.addValidator('is_upper_case', function (value, requirement) {
	return value === value.toUpperCase();
})

