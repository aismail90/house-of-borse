$(document).ready(function() {
	
//	window.Parsley.addValidator('isUpperCase', {
//		requirementType: 'string',
//		validateString: function(value, requirement) {
//			console.log(requirement);
//			return requirement !== requirement.toUpperCase();
//	    },
//	    messages: {
//	      en: 'This value %s should be in uppercase',
//	    }
//	});
	
	add_validation_rules();
	
	
	
});

$('#pair_form').bind('submit', function (e) {
	if($('#pair_form').parsley().isValid()) {
		$('#pair').prop('disabled', false);
	}
});


function add_validation_rules() {
	$('#pair_form').parsley();
	$('#pair').attr({'data-parsley-required':'true', 'data-parsley-maxlength':'10', 'data-parsley-is_upper_case': 'true'});
	$('#type').attr({'data-parsley-required':'true'});
	$('#min_spread').attr({'data-parsley-type':'number'});
	$('#avg_spread').attr({'data-parsley-type':'number'});
	$('#im_factor').attr({'data-parsley-required':'true', 'data-parsley-type':'number'});
	$('#trading_hours').attr({'data-parsley-required':'true', 'data-parsley-type':'integer'});
	$('#tick_factor').attr({'data-parsley-required':'true', 'data-parsley-type':'number'});
	$('#min_trade_size').attr({'data-parsley-type':'integer'});
	$('#contract_size_lot').attr({'data-parsley-type':'integer'});
}