$('.btn_quick_edit').bind('click', function() {
	var pair_id = $(this).attr('pair_id');
	$('.btn_quick_edit[pair_id='+pair_id+']').addClass('hidden');
	$('.btn_save[pair_id='+pair_id+']').removeClass('hidden');
	$('.btn_cancel[pair_id='+pair_id+']').removeClass('hidden');
	
	updateTDInputs(pair_id);
	
	showTDInputs(pair_id);
	hideTDText(pair_id);
});

$('.btn_cancel').bind('click', function() {
	var pair_id = $(this).attr('pair_id');
	$('.btn_quick_edit[pair_id='+pair_id+']').removeClass('hidden');
	$('.btn_save[pair_id='+pair_id+']').addClass('hidden');
	$('.btn_cancel[pair_id='+pair_id+']').addClass('hidden');
	
	hideTDInputs(pair_id);
	showTDText(pair_id);
});

$('.btn_save').bind('click', function() {
	var pair_id = $(this).attr('pair_id');
	$('.btn_quick_edit[pair_id='+pair_id+']').removeClass('hidden');
	$('.btn_save[pair_id='+pair_id+']').addClass('hidden');
	$('.btn_cancel[pair_id='+pair_id+']').addClass('hidden');
	
	saveNewValues(pair_id);
	
});

function hideTDInputs(id)
{
	$('.td_npt_'+id).addClass('hidden');
}

function hideTDText(id)
{
	$('.td_txt_'+id).addClass('hidden');
}

function showTDInputs(id)
{
	$('.td_npt_'+id).removeClass('hidden');
}

function showTDText(id)
{
	$('.td_txt_'+id).removeClass('hidden');
}

function updateTDInputs(id)
{
	$('#npt_min_spread_'+id).val($('#td_txt_min_spread_'+id).html());
	$('#npt_avg_spread_'+id).val($('#td_txt_avg_spread_'+id).html());
	$('#npt_im_factor_'+id).val($('#td_txt_im_factor_'+id).html());
//	$('#npt_trading_hours_'+id).val($('#td_txt_trading_hours_'+id).html());
	$('#npt_trading_hours_'+id).val($('#td_span_trading_hours_'+id).html());
//	$('#npt_value_date_'+id).val($('#td_txt_value_date_'+id).html());
	$('#npt_value_date_'+id).val($('#td_span_value_date_'+id).html());
	$('#npt_tick_factor_'+id).val($('#td_txt_tick_factor_'+id).html());
	$('#npt_min_trade_size_'+id).val($('#td_txt_min_trade_size_'+id).html());
	$('#npt_contract_size_lot_'+id).val($('#td_txt_contract_size_lot_'+id).html());
}

function updateTDText(id)
{
	$('#td_txt_min_spread_'+id).html($('#npt_min_spread_'+id).val());
	$('#td_txt_avg_spread_'+id).html($('#npt_avg_spread_'+id).val());
	$('#td_txt_im_factor_'+id).html($('#npt_im_factor_'+id).val());
//	$('#td_txt_trading_hours_'+id).html($('#npt_trading_hours_'+id).val());
	$('#td_span_trading_hours_'+id).html($('#npt_trading_hours_'+id).val());
//	$('#td_txt_value_date_'+id).html($('#npt_value_date_'+id).val());
	$('#td_span_value_date_'+id).html($('#npt_value_date_'+id).val());
	$('#td_txt_tick_factor_'+id).html($('#npt_tick_factor_'+id).val());
	$('#td_txt_min_trade_size_'+id).html($('#npt_min_trade_size_'+id).val());
	$('#td_txt_contract_size_lot_'+id).html($('#npt_contract_size_lot_'+id).val());
}

function saveNewValues(id)
{
	var data = {
		pair_id:			id,			
		pair: 				$('#td_txt_pair_'+id).html(),
		type: 				$('#td_txt_type_'+id).attr('type'),
		min_spread:			$('#npt_min_spread_'+id).val(),
		avg_spread:			$('#npt_avg_spread_'+id).val(),
		im_factor:			$('#npt_im_factor_'+id).val(),
		trading_hours:		$('#npt_trading_hours_'+id).val(),
		value_date:			$('#npt_value_date_'+id).val(),
		tick_factor:		$('#npt_tick_factor_'+id).val(),
		min_trade_size:		$('#npt_min_trade_size_'+id).val(),
		contract_size_lot:	$('#npt_contract_size_lot_'+id).val()
	};
	
	$.ajax({
		type:'GET',
 		url: $('#ajaxUpdatePairs').val(),
 		data: data,
 		dataType: 'JSON',
 		async: false,
 		success: function(response) {
 			if(response) {
 				console.log(response);
 				if(response.code == 200) {
 					$('tr[data-entry-id='+id+']').addClass('success').delay(1000).removeClass('success');
 					$('tr[data-entry-id='+id+']').addClass('success');
 		 			setTimeout(function() { $('tr[data-entry-id='+id+']').removeClass('success'); }, 1000);
 					updateTDText(id);
 					hideTDInputs(id);
 					showTDText(id);
 				} else {
 					$('.btn_cancel[pair_id='+id+']').trigger('click');
 					$('tr[data-entry-id='+id+']').addClass('danger');
 		 			setTimeout(function() { $('tr[data-entry-id='+id+']').removeClass('danger'); }, 1000);
 				}
 			} else {
 				$('.btn_cancel[pair_id='+id+']').trigger('click');
 				$('tr[data-entry-id='+id+']').addClass('danger');
 	 			setTimeout(function() { $('tr[data-entry-id='+id+']').removeClass('danger'); }, 1000);
 			}
 		},
 		error: function() {
 			$('.btn_cancel[pair_id='+id+']').trigger('click');
 			$('tr[data-entry-id='+id+']').addClass('danger');
 			setTimeout(function() { $('tr[data-entry-id='+id+']').removeClass('danger'); }, 1000);
 		}
 	});
}