/**
 * Created by root on 2/19/17.
 */
$('document').ready(function(){
    addValidationRole();
});

function  addValidationRole(){
    var form = $('#crud_form');
    form.parsley();
    $('#name').attr({'data-parsley-required':'true', 'data-parsley-maxlength':'255'});
    if(form.attr('model_name') == 'admin/user'){
        $('#email').attr({'data-parsley-required':'true'});
        if(form.attr('route_name') == 'crud.user.create'){
            $('#password').attr({'data-parsley-required':'true','data-parsley-minlength':'6'});
            $('#password_confirmation').attr({'data-parsley-required':'true','data-parsley-equalto':'#password'});
        }
        else{
            $('#password').attr({'data-parsley-minlength':'6'});
            $('#password_confirmation').attr({'data-parsley-equalto':'#password'});

        }


    }

}