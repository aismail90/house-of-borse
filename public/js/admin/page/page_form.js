$(document).ready(function() {

	add_validation_rules();
	
	CKEDITOR.on( 'instanceReady', function() {
	    $(".cke_dialog_ui_button").text("Upload");
		$.each( CKEDITOR.instances, function(instance) {
			CKEDITOR.instances[instance].document.on("keyup",
					CK_jQ);
			CKEDITOR.instances[instance].document.on("change",
					CK_jQ);
		} );
	} );
	var _plugins_path = $("#ckeditor_path").val() + '/plugins';

	CKEDITOR.on('dialogDefinition', function( ev ){
		var dialogName = ev.data.name;  
		var dialogDefinition = ev.data.definition;

		if ( dialogName === 'image' ){
			dialogDefinition.maxHeight = '100px';
			dialogDefinition.minHeight = '100px';
			dialogDefinition.removeContents('Link');
			dialogDefinition.removeContents('advanced');

			var uploadTab = dialogDefinition.getContents('Upload');
			var uploadButton = uploadTab.get('uploadButton');

			uploadButton.label = uploadTab.label = 'Upload Image';
			
//			filebrowser: {
//			    onSelect: function( fileUrl, data ) {
//			        alert( 'Successfully uploaded: ' + fileUrl );
//			    }
//			uploadButton['filebrowser']['onSelect'] = function( fileUrl, data ) {
//		        alert( 'Successfully uploaded Successfully uploaded Successfully uploaded Successfully uploaded: ' + fileUrl );
//		    };
//			console.log(uploadButton.filebrowser);
//			uploadButton['filebrowser']['onSelect'] = function( fileUrl, errorMessage ) {
//				alert('ranaaaaaaa');
//	        	$("input.cke_dialog_ui_input_text").val(fileUrl);
//	     		$(".cke_dialog_ui_button_ok span").click();
//	     		return false;
//			}

			dialogDefinition.onShow = function () {
				this.selectPage('Upload');
			};

			var infoTab = dialogDefinition.getContents('info');

//			infoTab.label = translations.image_link;

			infoTab.remove('txtAlt');
			infoTab.remove('browse');
			infoTab.remove('basic');
			infoTab.remove('htmlPreview');
		} else if (dialogName == 'link'){
			// Adjust dialog dimensions
			dialogDefinition.maxHeight = '100px';
			dialogDefinition.minHeight = '100px';

			// Change width of url input
			var infoTab = dialogDefinition.getContents('info');
			var urlOptions = infoTab.get('urlOptions');
			urlOptions.children[0].widths = ['0%','100%'];

		 	dialogDefinition.dialog.on('selectPage', function(e) {
	            if (e.data.page == 'target'){
					// Disable target change
	            	var elem = e.sender.getContentElement('target','linkTargetType');
	            	elem.disable();
	            } else if (e.data.page == 'info'){
	            	// Hide linkType and protocol
					var elem = e.sender.getContentElement('info','linkType');
					elem.getElement().hide();

					var elem = e.sender.getContentElement('info','protocol');
					elem.getElement().hide();
	            }
	        });

		 	// Make target default blank
			var targetTab = dialogDefinition.getContents('target');
			var targetOption = targetTab.get('linkTargetType');
			targetOption['default'] = '_blank';

		}
	});

	CKEDITOR.config.removePlugins = 'imgupload';
	CKEDITOR.config.forcePasteAsPlainText = true;
//	CKEDITOR.config.extraAllowedContent = 'div(*)';
	CKEDITOR.config.allowedContent = true;
//	CKEDITOR.config.allowedContent = 'a[*] {text-align}; p strong div em ol li ul {text-align}; img[*]{text-align}; iframe[*]{text-align};';
	
	
	CKEDITOR.config.autoParagraph = false;
	
	CKEDITOR.config.contentsCss = $('#content_css_url').val(); 
	
	CKEDITOR.config.stylesSet = [
	                             { name: 'Underline Header', element: 'span', attributes: { 'class': 'line' } },
	                             
	                             { name: 'HOB Listing', element: 'ul', attributes: { 'class': 'hob-ul' } }
	                             
//	                             { name: 'Marker: Yellow', element: 'span', styles: { 'background-color': 'Yellow' } },
	                            
//	                             // Object Styles
//	                             {
//	                                 name: 'Image on Left',
//	                                 element: 'img',
//	                                 attributes: {
//	                                     style: 'padding: 5px; margin-right: 5px',
//	                                     border: '2',
//	                                     align: 'left'
//	                                 }
//	                             }
     ];
	
	CKEDITOR.dtd.$removeEmpty['span'] = false;
	CKEDITOR.dtd.$removeEmpty['i'] = false
	
	CKEDITOR.replace('content', {
//		filebrowserImageBrowseUrl: _plugins_path + "/imgbrowse/imgbrowse.html?imgroot=" + $("#upload_browse_url").val(),
		filebrowserImageUploadUrl: _plugins_path + "/imgupload/imgupload.php?imgroot=" + $("#upload_browse_url").val()
	});

});

$('#slug_btn_edit').bind('click', function() {
	$('#slug_lnk').html($('#hdn_link_val').val()+'/');
	$('#slug_input').removeClass('hidden');
	$('#slug_btn_edit').addClass('hidden');
	$('#slug_btn_ok').removeClass('hidden');
	$('#slug_btn_cancel').removeClass('hidden');
});

$('#slug_btn_cancel').bind('click', function() {
	$('#slug_lnk').html($('#slug_lnk').attr('href'));
	$('#slug_input').addClass('hidden');
	$('#slug_btn_ok').addClass('hidden');
	$('#slug_btn_cancel').addClass('hidden');
	$('#slug_btn_edit').removeClass('hidden');
});

$('#slug_btn_ok').bind('click', function() {
	if($.trim($('#slug_input').val()) !== '') {
		
		var data = {};
		data.slug 		= $.trim($('#slug_input').val());
		data.page_id 	= $('#page_id').val(); 
		data.locale		= $('#language').val();
		
		
		$.ajax({
			type:'GET',
	 		url: $('#ajaxcheckIfSlugExistsUrl').val(),
	 		data: data,
	 		dataType: 'JSON',
	 		async: false,
	 		success: function(response) {
	 			if(response) {
	 				if(response.code == 200) {
	 					$('#slug_lnk').html(response.href);
	 					$('#slug_lnk').attr('href', response.href);
	 					$('#hdn_link_val').val(response.link);
	 					$('#slug_input').val(response.slug);
	 					$('#hdn_slug_val').val(response.slug);
	 					$('#slug_input').addClass('hidden');
	 					$('#slug_btn_ok').addClass('hidden');
	 					$('#slug_btn_cancel').addClass('hidden');
	 					$('#slug_btn_edit').removeClass('hidden');
	 				}
	 			}
	 		}
	 	});
		
	} 
});

$('.preview_page').bind('click', function(e) {
	e.preventDefault();
	CKupdate();
//	$('#language').prop("disabled", false);
	var url = $('#ajaxPagePreviewUrl').val();
	$.ajax({
        type: 'POST',
        url: url,
        data: $('#page_form').serialize(),
        dataType: 'JSON',
        success: function (response) {
//        	$('#language').prop("disabled", true);
            if(response.code == 200) {
            	$('#page_id').val(response.page_id);
            	window.open(
        			$('#slug_lnk').attr('href')+'/preview',
        			'_blank'
	          	);
            }
        }
    });
});

$('#page_form').bind('submit', function (e) {
	if($('#page_form').parsley().isValid()) {
		$('#language').prop('disabled', false);
	}
});

function CK_jQ() {
	for (instance in CKEDITOR.instances) {
		CKEDITOR.instances[instance].updateElement();
	}
}

function CKupdate() {
    for ( instance in CKEDITOR.instances )
        CKEDITOR.instances[instance].updateElement();
}

function add_validation_rules() {
	$('#page_form').parsley();
	$('#title').attr({'data-parsley-required':'true', 'data-parsley-maxlength':'255'});
	$('#status').attr({'data-parsley-required':'true'});
	$('#language').attr({'data-parsley-required':'true'});
	$('#parent').attr({'data-parsley-required':'true'});
}