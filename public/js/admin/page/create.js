$(document).ready(function() {
	
});

var change_premlink = false;

$('#title').bind('blur', function() {
	if($.trim($('#title').val()) !== '') {
		
		change_premlink = true;
		
		createPageSlug();
		
		$('#title').unbind('blur');
	}
});

function createPageSlug()
{
	var data = {};
	data.title 	= $.trim($('#title').val());
	data.locale	= $('#language').val();
	
	$.ajax({
		type:'GET',
 		url: $('#ajaxCreatePageSlugUrl').val(),
 		data: data,
 		dataType: 'JSON',
 		async: false,
 		success: function(response) {
 			if(response) {
 				if(response.code == 200) {
 					$('#slug_lnk').html(response.href);
 					$('#slug_lnk').attr('href', response.href);
 					$('#hdn_link_val').val(response.link);
 					$('#slug_input').val(response.slug);
 					$('#hdn_slug_val').val(response.slug);
 					$('#slug_grp').removeClass('hidden');
// 					$('#title').unbind('blur');
 					$('#preview_btn').removeClass('hidden');
 				}
 			}
 		}
 	});
}