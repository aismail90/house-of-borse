<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 3/15/17
 * Time: 4:55 PM
 */

namespace App\Managers;


use App\Models\Country;

class CountryManager {
 public function cacheCountries(){
    $countries=Country::getAllCountries();
//     Cache::forever('countries', $countries);
     return $countries;

 }
 public function getCountryByCode($iso3){
     $country= Country::getCountryByCode($iso3);
     return $country;
 }

} 