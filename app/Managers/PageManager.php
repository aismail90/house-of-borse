<?php 

namespace App\Managers;

use App\Models\Page;
use App\Models\PageData;
use App\Enum\GeneralEnum;

class PageManager {
	
	public function selectLanguagesOfPage($page_id, $translate = FALSE)
	{
		$languages = [];
		$pages_data = PageData::select('lang')->where('page_id', $page_id)->where('preview', GeneralEnum::$page_preview['false'])->get()->toArray();
		if($pages_data) {
			foreach ($pages_data as $lang) {
				if(!$translate) {
					array_push($languages, $lang['lang']);
				} else {
					$languages[$lang['lang']] = trans('admin.'.$lang['lang']);
				}
				
			}
		}
	
		return $languages;
	}
	
	public function getPageTypes()
	{
		return GeneralEnum::$page_type;
	}
	
	public function checkIfSlugExists($slug, $page_id = FALSE)
	{
		$page = Page::whereSlug($slug)
				->where(function($query) use ($page_id)  {
					if($page_id) {
						$query->where('id', '!=', $page_id);
					}
				})
				->first();
		if($page) {
			$slug = $slug.'-1';
			$slug = $this->createSlugIfExists($slug);
		}
		
		$slug = $this->createSlug($slug);
		
		if($page_id) {
			Page::where('id', $page_id)->update(['slug' => $slug]);
		}
		
		return $slug;
	}
	
	public function createSlugIfExists($slug)
	{
		$page = Page::whereSlug($slug)->first();
		if($page) {
			$replacement = substr($slug, -1)+1;
			$slug = substr($slug, 0, -1).$replacement;
			$slug = $this->createSlugIfExists($slug);
		}
		return $slug;
	}
	
	public function createPageSlug($title)
	{
		if(!empty(trim($title))) {
			$slug = $this->createSlug($title);
			if($slug) {
				$page = Page::whereSlug($slug)->first();
				if($page) {
					$slug = $slug.'-1';
					$slug = $this->createSlugIfExists($slug);
				}
				return $slug;
			}
		}
		return FALSE;
	}
	
	private function checkPageInputs($inputs, $is_ajax = FALSE)
	{
		if(empty($inputs['status']) || $is_ajax) {
			$inputs['status'] = array_search('preview', GeneralEnum::$page_status);
		}
		if(empty($inputs['language'])) {
			$inputs['language'] = config('app.fallback_locale');
		}
		if(empty($inputs['parent'])) {
			$inputs['parent'] = 0;
		}
		return $inputs;
	}
	
	private function insertUpdatePage($inputs)
	{
		$where = ['slug' => $inputs['slug']];
		if(array_key_exists('page_id', $inputs) && !empty($inputs['page_id'])) {
			$where['id'] = $inputs['page_id'];
		}
		$page 			= Page::updateOrCreate($where, ['status' => $inputs['status'], 'parent' => $inputs['parent']]);
		return $page;
	}
	
	private function insertUpdatePageData($inputs, $page_id)
	{
		$insert_or_update = [
			'title'			=> $inputs['title'],
			'brief'			=> $inputs['brief'],
			'content'		=> $inputs['content'],
			'status'		=> $inputs['status'],
			'created_by'	=> \Auth::user()->id,
			'updated_by'	=> \Auth::user()->id
		];
		$page_data 				= PageData::updateOrCreate(['page_id' => $page_id, 'lang' => $inputs['language'], 'preview' => GeneralEnum::$page_preview['false']], $insert_or_update);
		return $page_data;
	}
	
	private function insertUpdatePreviewData($inputs, $page_id)
	{
		$insert_or_update = [
			'title'			=> $inputs['title'],
			'brief'			=> $inputs['brief'],
			'content'		=> $inputs['content'],
			'status'		=> array_search('preview', GeneralEnum::$page_status),
			'created_by'	=> \Auth::user()->id,
			'updated_by'	=> \Auth::user()->id
		];
		$page_data 	= PageData::updateOrCreate(['page_id' => $page_id, 'lang' => $inputs['language'], 'preview' => GeneralEnum::$page_preview['true']], $insert_or_update);
		return $page_data;
	}
	
	
	public function savePage($inputs, $is_ajax = FALSE)
	{
		$inputs = $this->checkPageInputs($inputs, $is_ajax);
		
		if(array_key_exists('page_id', $inputs) && !empty($inputs['page_id']) && $is_ajax) {
			$preview = $this->insertUpdatePreviewData($inputs, $inputs['page_id']);
			return $inputs['page_id'];
		}
		
		$page 			= $this->insertUpdatePage($inputs);
		if($page) {
			
			if($is_ajax) {
				$preview = $this->insertUpdatePreviewData($inputs, $page->id);
				return $preview->page_id;
			}
			
			$page_data 	= $this->insertUpdatePageData($inputs, $page->id);
			if($page_data) {
				$this->insertUpdatePreviewData($inputs, $page->id);
				return $this->mergePageData($page, $page_data);
			}
		}
		
		return FALSE;
	}
	
	public function updatePage($inputs, $locale, $id)
	{
		$page 			= Page::find($id);
		$page->status 	= $inputs['status'];
		$page->slug		= $inputs['slug'];
		$page->parent	= $inputs['parent'];
		if(trim(empty($inputs['slug']))) {
			$page->slug = $this->createPageSlug($inputs['title']);
		}
		$save_page 		= $page->save();
		if($save_page) {
			$insert_or_update = [
					'title'			=> $inputs['title'],
					'brief'			=> $inputs['brief'],
					'content'		=> $inputs['content'],
					'status'		=> $inputs['status'],
					'created_by'	=> \Auth::user()->id,
					'updated_by'	=> \Auth::user()->id
			];
			$page_data 				= PageData::updateOrCreate(['page_id' => $page->id, 'lang' => $inputs['language']], $insert_or_update);
			if($page_data) {
				return $this->mergePageData($page, $page_data);
			}
		}
	
		return FALSE;
	}
	
	public function getParentPages()
	{
		$parent_pages = GeneralEnum::$parent_pages;
		foreach ($parent_pages as $key => $val) {
			$parent_pages[$key] = trans('portal.'.$val['trans']);
		}
		return $parent_pages;
	}
	
	public function getPages($locale, $user_id = FALSE)
	{
		if(!empty($user_id)) {
			$pages 		= Page::findPages(['user_id' => $user_id]);
		} else {
			$pages 		= Page::findPages();
		}
		
		if(!empty($pages)) {
			$data = [];
			foreach ($pages as $page) {
				if(!array_key_exists($page->id, $data)) {
					$data[$page->id] = [
						'page_data_id'	=> $page->page_data_id,
// 						'title'			=> $page->title,
						'parent'		=> $page->parent,
						'parent_lbl'	=> trans('portal.'.GeneralEnum::$parent_pages[$page->parent]['trans']),
						'status'		=> $page->status,
						'status_lbl'	=> trans('admin.'.GeneralEnum::$page_status[$page->status]),
						'slug'			=> $page->slug,
						'lang'			=> [$page->lang],
						'current_lang'	=> $page->lang,
						'created_by'	=> $page->created_by	
					];
				} else {
					array_push($data[$page->id]['lang'], $page->lang);
				}
				if($page->lang == $locale && isset($page->title)) {
					$data[$page->id]['title'] = $page->title;
				}
			}
			return $data;
		}
		
		return FALSE;
	}
	
	private function createSlug($str , $maxLength = 1000 , $rc = '-')
	{
		//$str = preg_replace('/[^\x{0600}-\x{06FF}A-Za-z !@#$%^&*()]/u','',$str);
		$str = strtolower(trim($str));
		$str = preg_replace('/-+/', $rc , $str);
		$str = preg_replace("/[\s-]+/", $rc , $str);
		$str = preg_replace("/\s/", "-", $str);
		$special_chars = array("?","%", "." , "[", "]", "/", "\\", "=", "<", ">", ":", ";", ",", "'", "\"", "&", "$", "#", "*", "(", ")", "|", "~", "`", "!", "{", "}","+");
		$str = str_replace($special_chars, '', $str);
		$str = trim(substr($str, 0, $maxLength));
		return $str;
	}
	
	public function getLanguages($locale = FALSE, $extract = [])
	{
		$data = [];
		$languages = GeneralEnum::$languages;
		foreach ($languages as $key => $val) {
			if($locale && $locale == $key) {
				return [$key => trans('admin.'.$key)];
			}
			if(!in_array($key, $extract)) { 
				$data[$key] = trans('admin.'.$key);
			}
			
		}
		return $data;
	}
	
	public function getPageStatus()
	{
		$status = GeneralEnum::$page_status;
		foreach ($status as $key => $val) {
			$status[$key] = trans('admin.'.$val);
		}
		return $status;
	}
	
	private function mergePageData($page, $page_data)
	{
		if(!empty($page) && !empty($page_data)) {
			$data = [
				'id'			=> $page->id,
				'page_data_id'	=> $page_data->id,	
				'lang'			=> $page_data->lang,
				'title'			=> $page_data->title,
				'content'		=> $page_data->content,
				'slug'			=> $page->slug,
				'status'		=> $page->status,
				'parent'		=> $page->parent,
			];
			return $data;
		}
		
		return FALSE;
	}
	
	public function getPageDetails($locale, $field = 'id', $val, $preview = FALSE)
	{
		if($field == 'id') {
			if(!is_int($val)) { return FALSE; }
		} elseif ($field == 'slug') {
			if(!is_string($val)) { return FALSE; }
		} else {
			return FALSE;
		}
	
		$conditions = [$field => $val, 'locale' => $locale];
		if(!$preview) {
			$conditions['status'] = array_search('publish', GeneralEnum::$page_status);
		} else {
			$conditions['preview'] = GeneralEnum::$page_preview['true'];
		}
		$page = Page::findPageDetails($conditions);
		if($page) {
			return [
				'id'			=> $page->id,
				'page_data_id'	=> $page->page_data_id,	
				'lang'			=> $page->lang,
				'title'			=> $page->title,
				'brief'			=> $page->brief,
				'content'		=> $page->content,
				'slug'			=> $page->slug,
				'status'		=> $page->status,
				'parent'		=> $page->parent,
				'created_by'	=> $page->created_by	
			];
		}
	
		return FALSE;
	}
	
}