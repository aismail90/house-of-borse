<?php 

namespace App\Managers;

use App\Models\Pair;
use App\Enum\GeneralEnum;

class PairManager {
	
	
	public function getPairTypes()
	{
		$types = GeneralEnum::$pair_type;
		foreach ($types as $key => $val) {
			$types[$key] = trans('admin.'.$val);
		}
		return $types;
	}
	
	public function getPairs()
	{
		$pairs 		= Pair::all();
		
		if(!empty($pairs)) {
			$data = [];
			foreach ($pairs as $pair) {
				$data[$pair->id] = $this->getPairData($pair);
			}
			return $data;
		}
		
		return FALSE;
	}
	
	public function getPairDetails($id)
	{
		$pair = Pair::find($id);
		if($pair) {
			return $this->getPairData($pair);
		}
	
		return FALSE;
	}
	
	private function getPairData($pair)
	{
		if(!empty($pair)) {
			$data = [
				'id'				=> $pair->id,
				'pair'				=> $pair->pair,
				'type'				=> $pair->type,	
				'type_lbl'			=> trans('admin.'.GeneralEnum::$pair_type[$pair->type]),
				'min_spread'		=> $pair->min_spread,
				'avg_spread' 		=> $pair->avg_spread,
				'im_factor' 		=> $pair->im_factor,
				'trading_hours' 	=> $pair->trading_hours,
				'value_date' 		=> $pair->value_date,
				'tick_factor' 		=> $pair->tick_factor,
				'min_trade_size' 	=> $pair->min_trade_size,
				'contract_size_lot' => $pair->contract_size_lot,
			];
			return $data;
		}
	
		return FALSE;
	}
	
	private function insertUpdatePair($inputs)
	{
		$where = ['pair' => $inputs['pair']];
		if(array_key_exists('pair_id', $inputs) && !empty($inputs['pair_id'])) {
			$where['id'] = $inputs['pair_id'];
		}
		$data = [
			'type'				=> $inputs['type'],
			'min_spread'		=> $inputs['min_spread'],
			'avg_spread'		=> $inputs['avg_spread'],
			'im_factor'			=> $inputs['im_factor'],
			'trading_hours'		=> $inputs['trading_hours'],
			'value_date'		=> $inputs['value_date'],
			'tick_factor'		=> $inputs['tick_factor'],
			'min_trade_size'	=> $inputs['min_trade_size'],
			'contract_size_lot'	=> $inputs['contract_size_lot']
		];
		$pair 			= Pair::updateOrCreate($where, $data);
		return $pair;
	}
	
	public function savePair($inputs)
	{
		$pair 			= $this->insertUpdatePair($inputs);
		if($pair) {
			return $this->getPairData($pair);
		}
		
		return FALSE;
	}
	
	public function updatePair($inputs)
	{
		$pair 			= $this->insertUpdatePair($inputs);
		if($pair) {
			return $this->getPairData($pair);
		}
		
		return FALSE;
	}
	
}