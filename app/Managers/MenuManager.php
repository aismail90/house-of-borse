<?php 

namespace App\Managers;

class MenuManager 
{
	public function generateMenu($curr_path = FALSE)
	{
		$content = file_get_contents(config('app.menu_path'));
		$menu_decoded = json_decode($content, TRUE);
		$menu_data = $menu_decoded['menu'];
	
		$menu 		= [];
		$temp 		= [];
		$sub_menu	= [];
		
		$breadcrumb 		= [];
		$breadcrumb_temp 	= [];
		
		foreach ($menu_data as $data) {
			if($data['enable'] == "TRUE") {
				
				$temp = [
					'title'		=> $data['title'],
					'route'		=> $data['route'],
					'path'		=> $data['path'],
					'url'		=> $data['url'],
					'external'	=> $data['external'],
					'submenu'	=> []	
				];
				
				if(array_key_exists('submenu', $data) && !empty($data['submenu'])) {
					foreach($data['submenu'] as $submenu) {
						if($submenu['enable'] == "TRUE") {
							$sub_menu = [
								'title'		=> $submenu['title'],
								'route'		=> $submenu['route'],
								'path'		=> $submenu['path'],
								'url'		=> $submenu['url'],
								'external'	=> $submenu['external']
							];
							if($curr_path == $submenu['path']) {
								$temp['active'] = TRUE;
								$sub_menu['active'] = TRUE;
								
								// breadcrumb
								$breadcrumb_temp = ['title' => $data['title']];
								if(!empty($temp['path'])) {
									$breadcrumb_temp['path'] = $data['path'];
								}
								array_push($breadcrumb, $breadcrumb_temp);
								
								$breadcrumb_temp = ['title' => $submenu['title']];
								$breadcrumb_temp['active'] = TRUE;
								if(!empty($submenu['path'])) {
									$breadcrumb_temp['path'] = $submenu['path'];
								}
								array_push($breadcrumb, $breadcrumb_temp);
							}
							array_push($temp['submenu'], $sub_menu);
						}
					}
					
				}
				array_push($menu, $temp);
			}
		}
		
		$return = ['menu' => $menu, 'breadcrumb' => $breadcrumb];
		return $return;
	}
	
}