<?php


function get_daynamic_views_for_static_pages($matches)
{
	if(is_array($matches) && count($matches) > 1) {
		if(View::exists('elements/pages/'.$matches[1])) {
			return View('elements/pages/'.$matches[1])->render();
		}
	}
	return '';
}

function checkForElementReplacements($content)
{
	if(empty($content)) return $content;
	
	echo preg_replace_callback('#<code>(.*?)</code>#is', 'get_daynamic_views_for_static_pages', $content);
}

function getLiveFXRates($symbols = FALSE)
{
	try {
		$content = file_get_contents(config('app.fx_rates_json_path'));
		if($content) {
			$data_decoded = json_decode($content, TRUE);
			$symbols_data = $data_decoded['symbols'];
			if(!empty($symbols)) {
				$data = [];
				foreach ($symbols_data as $symbol) {
					if(in_array($symbol[0]['symbol'], $symbols)) {
						$data[$symbol[0]['symbol']] = $symbol;
					}
				}
				return $data;
			}
			return $symbols_data;
		}
	} catch (\Exception $e) {
		return FALSE;
	}
	
	return FALSE;
}

function addSlashToCurrency($symbol)
{
	$pos = strpos($symbol, '/');
	if ($pos === false) {
		return getBaseCurrency($symbol).'/'.getQuoteCurrency($symbol);
	} else {
		return $symbol;
	}
}

function getBaseCurrency($symbol)
{
	$symbol = cleanCurrencySymbol($symbol);
	return  substr($symbol, 0, 3);
}

function getQuoteCurrency($symbol)
{
	$symbol = cleanCurrencySymbol($symbol);
	return  substr($symbol, 3);
}

function cleanCurrencySymbol($symbol)
{
	$pos = strpos($symbol, '/');
	if ($pos === false) {
		return $symbol;
	} else {
		return str_replace('/', '', $symbol);
	}
}

function countDecimalPlaces($number)
{
	if(empty($number)) { return 0; }
	return strlen(substr(strrchr(floatval($number), "."), 1));
}

function addZeroes($num, $decimal_places)
{
	if(countDecimalPlaces($num) < $decimal_places) {
		return number_format($num, $decimal_places);
	}
	return $num;
}

function getCountries(){

        $countries=app('CountryManager')->cacheCountries();
        return $countries;
}

function getCurrentCountry($ip){
    $iso3 = geoip_country_code3_by_name($ip);
    if(!$iso3) $iso3='EGY';
    return $iso3;

}

