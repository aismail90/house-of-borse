<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pair extends Model
{
	protected $fillable = ['pair', 'type', 'min_spread', 'avg_spread', 'im_factor', 'trading_hours', 'value_date', 'tick_factor', 'min_trade_size', 'contract_size_lot'];
	
	public $timestamps = false;
}
