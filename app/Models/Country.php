<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 3/15/17
 * Time: 3:20 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Country extends Model{
protected $table="countries";

    public static Function getAllCountries(){
        $countries =  \DB::table('countries')->select('id','name','nameenglish','ISO2','ISO3','dialing_prefix')->get()->toArray();
        return $countries;

    }



} 