<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageData extends Model
{
	
	protected $fillable = ['page_id', 'lang', 'title',  'brief', 'content', 'status', 'preview', 'created_by', 'updated_by'];
	
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table = 'pages_data';
}
