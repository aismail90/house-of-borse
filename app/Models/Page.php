<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Enum\GeneralEnum;

class Page extends Model
{
	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $fillable = ['status', 'parent', 'slug', 'order'];
	
	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = false;
	
	public static function findPages($conditions = [])
	{
		$pages = DB::table('pages')
			->join('pages_data', 'pages.id', '=', 'pages_data.page_id')
			->select('pages.id', 'pages.status', 'pages.parent', 'pages.slug', 'pages_data.id AS page_data_id', 'pages_data.title', 'pages_data.lang', 'pages_data.created_by')
			->where('pages_data.preview', GeneralEnum::$page_preview['false'])
			->where(function($query) use ($conditions)  {
				if(!empty($conditions)) {
					if(array_key_exists('locale', $conditions)) {
						$query->where('pages_data.lang', $conditions['locale']);
					}
					if(array_key_exists('user_id', $conditions)) {
						$query->where('pages_data.created_by', $conditions['user_id']);
					}
				}
			})
			->get()->toArray();
		
		if(!empty($pages)) {
			return $pages;
		}
			
		return FALSE;
	}
	
	public static function findPageDetails($conditions = [])
	{
// 		DB::enableQueryLog();
		
		$page = DB::table('pages')
		->join('pages_data', 'pages.id', '=', 'pages_data.page_id')
		->select('pages.id', 'pages.status', 'pages.parent', 'pages.slug', 'pages_data.id AS page_data_id', 'pages_data.title', 'pages_data.content', 'pages_data.lang', 'pages_data.brief', 'pages_data.created_by')
// 		->where('pages_data.preview', GeneralEnum::$page_preview['false'])
		->where(function($query) use ($conditions)  {
			if(!empty($conditions)) {
				if(array_key_exists('locale', $conditions)) {
					$query->where('pages_data.lang', $conditions['locale']);
				}
				if(array_key_exists('id', $conditions)) {
					$query->where('pages.id', $conditions['id']);
				} else {
					$query->where('pages.slug', $conditions['slug']);
					
					if(array_key_exists('status', $conditions)) {
						$query->where('pages.status', $conditions['status']);
					}
// 					if(array_key_exists('locale', $conditions)) {
// 						$query->where('pages_data.lang', $conditions['locale']);
// 					}
					if(array_key_exists('preview', $conditions)) {
						$query->where('pages_data.preview', $conditions['preview']);
					}
// 					if(array_key_exists('locale', $conditions)) {
// 						$query->where('pages_data.lang', $conditions['locale']);
// 					}
				}
				
			}
		})
		->get()->toArray();
		
// 		var_dump(DB::getQueryLog());
	
		if(!empty($page)) {
			return $page[0];
		}
			
		return FALSE;
	}
	
}
