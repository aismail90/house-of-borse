<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Enum\GeneralEnum;

class HomePageController extends Controller
{
	
	function __construct()
	{
		parent::__construct();
	}
	
    public function index()
    {
    	$this->data['body_class'] = 'home-template';
    	
    	$data = getLiveFXRates(GeneralEnum::$spread_symbols);
    	if(!empty($data)) {
//     		var_dump($data['EURUSD']);
    		$this->data['spread'] = $data;
    	}
    	
    	$this->data['curr_pairs'] 	= implode(',', GeneralEnum::$spread_symbols);
    	
    	return View('homepage', $this->data);
    }
    
}
