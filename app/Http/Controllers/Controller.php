<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Enum\GeneralEnum;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    protected $data;
    protected $locale;
    protected $request ;
    
    function __construct()
    {
    	$locale 				= \App::getLocale();
    	$this->data['locale'] 	= $locale;
    	$this->locale			= $locale;

    	$this->beforeFilter();

    }
    
    public function beforeFilter()
    {
    	$current_path = \Request::path();
    	$current_path = str_replace($this->locale, '', $current_path);
    	
    	$menu = App('MenuManager')->generateMenu($current_path);
    	$this->data['menu'] = $menu['menu'];
    	$this->data['breadcrumb'] = $menu['breadcrumb'];
    	
    	if($this->locale == 'ar') {
    		$this->data['css_driection'] = 'rtl';
    	} else {
    		$this->data['css_driection'] = 'ltr';
    	}
    	
    	$this->data['ddl_languages'] = GeneralEnum::$languages;
//     	$route_name = Route::currentRouteName();
//     	$route_param = Route::current()->parameters();
    	
    }




    
}
