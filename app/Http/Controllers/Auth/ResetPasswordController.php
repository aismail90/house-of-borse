<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;

class ResetPasswordController extends \Backpack\Base\app\Http\Controllers\Auth\ResetPasswordController
{
//    /*
//    |--------------------------------------------------------------------------
//    | Password Reset Controller
//    |--------------------------------------------------------------------------
//    |
//    | This controller is responsible for handling password reset requests
//    | and uses a simple trait to include this behavior. You're free to
//    | explore this trait and override any methods you wish to tweak.
//    |
//    */
//
//    use ResetsPasswords;
//
//    /**
//     * Create a new controller instance.
//     *
//     * @return void
//     */
//    public function __construct()
//    {
//        $this->middleware('guest');
//    }
//

    public function __construct()
    {
        $this->middleware('guest');

        // where to redirect after password was reset
        $this->redirectTo = property_exists($this, 'redirectTo') ? $this->redirectTo : \App::getLocale().'/'.config('backpack.base.route_prefix', 'admin').'/dashboard';
    }

    public function showResetForm(Request $request, $token = null)
    {
        return view('vendor.backpack.base.auth.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }
}
