<?php

namespace app\http\controllers\Admin\Backpack\LangFileManager;

use App\Http\Requests;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\LangFileManager\app\Services\LangFiles;
use Backpack\LangFileManager\app\Models\Language;
use Illuminate\Http\Request;
// VALIDATION: change the requests to match your own file names if you need form validation
use Backpack\LangFileManager\app\Http\Requests\LanguageRequest as StoreRequest;
use Backpack\LangFileManager\app\Http\Requests\LanguageRequest as UpdateRequest;

class LanguageCrudController extends \Backpack\LangFileManager\app\Http\Controllers\LanguageCrudController
{
    /**
     * Display all rows in the database for this entity.
     *
     * @return Response
     */
    public function index()
    {
        if (\Auth::user()->can('translate assets') || \Auth::user()->hasRole('admin')) {

        $this->crud->hasAccessOrFail('list');

        $this->data['crud'] = $this->crud;
        $this->data['title'] = ucfirst($this->crud->entity_name_plural);

        // get all entries if AJAX is not enabled
        if (! $this->data['crud']->ajaxTable()) {
            $this->data['entries'] = $this->data['crud']->getEntries();
        }

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        // $this->crud->getListView() returns 'list' by default, or 'list_ajax' if ajax was enabled
        return view('crud::list', $this->data);
        }
        else {
            return abort(401, 'You are not authorized to access this resource.');
        }
    }

    /**
     * Show the form for creating inserting a new row.
     *
     * @return Response
     */
    public function create()
    {
        if (\Auth::user()->can('translate assets') || \Auth::user()->hasRole('admin')) {
        $this->crud->hasAccessOrFail('create');

        // prepare the fields you need to show
        $this->data['crud'] = $this->crud;
        $this->data['fields'] = $this->crud->getCreateFields();
        $this->data['title'] = trans('backpack::crud.add').' '.$this->crud->entity_name;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view('crud::create', $this->data);
        } else {
            return abort(401, 'You are not authorized to access this resource.');
        }
    }


    public function store(StoreRequest $request)
    {
        if (\Auth::user()->can('translate assets') || \Auth::user()->hasRole('admin')) {
        $defaultLang = Language::where('default', 1)->first();

        // Copy the default language folder to the new language folder
        \File::copyDirectory(resource_path('lang/'.$defaultLang->abbr), resource_path('lang/'.$request->input('abbr')));

        $this->crud->hasAccessOrFail('create');

        // fallback to global request instance
        if (is_null($request)) {
            $request = \Request::instance();
        }

        // replace empty values with NULL, so that it will work with MySQL strict mode on
        foreach ($request->input() as $key => $value) {
            if (empty($value) && $value !== '0') {
                $request->request->set($key, null);
            }
        }

        // insert item in the db
        $item = $this->crud->create($request->except(['redirect_after_save', '_token']));
        $this->data['entry'] = $this->crud->entry = $item;

        // show a success message
        \Alert::success(trans('backpack::crud.insert_success'))->flash();

        // redirect the user where he chose to be redirected
        switch ($request->input('redirect_after_save')) {
            case 'current_item_edit':
                return \Redirect::to($this->crud->route.'/'.$item->getKey().'/edit');

            default:
                return \Redirect::to($request->input('redirect_after_save'));
        }
        } else {
            return abort(401, 'You are not authorized to access this resource.');
        }
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        if (\Auth::user()->can('translate assets') || \Auth::user()->hasRole('admin')) {
        $this->crud->hasAccessOrFail('update');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;

        $this->data['id'] = $id;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view('crud::edit', $this->data);
        }else {
                return abort(401, 'You are not authorized to access this resource.');
            }
    }

    public function update(UpdateRequest $request)
    {
        if (\Auth::user()->can('translate assets') || \Auth::user()->hasRole('admin')) {
        $this->crud->hasAccessOrFail('update');

        // fallback to global request instance
        if (is_null($request)) {
            $request = \Request::instance();
        }

        // replace empty values with NULL, so that it will work with MySQL strict mode on
        foreach ($request->input() as $key => $value) {
            if (empty($value) && $value !== '0') {
                $request->request->set($key, null);
            }
        }

        // update the row in the db
        $item = $this->crud->update($request->get($this->crud->model->getKeyName()),
            $request->except('redirect_after_save', '_token'));
        $this->data['entry'] = $this->crud->entry = $item;

        // show a success message
        \Alert::success(trans('backpack::crud.update_success'))->flash();

        return \Redirect::to($request->segment(1).'/'.$this->crud->route);
        } else {
            return abort(401, 'You are not authorized to access this resource.');
        }
    }

    /**
     * After delete remove also the language folder.
     *
     * @param int $id
     *
     * @return string
     */
    public function destroy($id)
    {
        if (\Auth::user()->can('translate assets') || \Auth::user()->hasRole('admin')) {
        $language = Language::find($id);
        $destroyResult = parent::destroy($id);

        if ($destroyResult) {
            \File::deleteDirectory(resource_path('lang/'.$language->abbr));
        }

        return $destroyResult;
        }
        else{
            return abort(401, 'You are not authorized to access this resource.');
        }
    }

    public function showTexts(LangFiles $langfile, Language $languages, $lang = '', $file = 'site')
    {


        if($lang && $languages->findByAbbr($lang)==null)
        {
           return abort(404);
        }
        // SECURITY
        // check if that file isn't forbidden in the config file
        if (\Auth::user()->can('translate assets') || \Auth::user()->hasRole('admin')) {
        if (in_array($file, config('backpack.langfilemanager.language_ignore'))) {
            abort('403', trans('backpack::langfilemanager.cant_edit_online'));
        }

        if ($lang) {
            $langfile->setLanguage($lang);
        }

        $langfile->setFile($file);
        $this->data['crud'] = $this->crud;
        $this->data['currentFile'] = $file;
        $this->data['currentLang'] = $lang ?: config('app.locale');
        $this->data['currentLangObj'] = Language::where('abbr', '=', $this->data['currentLang'])->first();
        $this->data['browsingLangObj'] = Language::where('abbr', '=', config('app.locale'))->first();
        $this->data['languages'] = $languages->orderBy('name')->get();
        $this->data['langFiles'] = $langfile->getlangFiles();
        $path =
        $this->data['fileArray'] = $this->getFileContent($lang,$file);
        $this->data['langfile'] = $langfile;
        $this->data['title'] = trans('backpack::langfilemanager.translations');

        return view('langfilemanager::translations', $this->data);
        }
        else{
            return abort(401, 'You are not authorized to access this resource.');
        }
    }

    public function updateTexts(LangFiles $langfile, Request $request, $lang = '', $file = 'site')
    {

        // SECURITY
        // check if that file isn't forbidden in the config file
    if (\Auth::user()->can('translate assets') || \Auth::user()->hasRole('admin')) {
        if (in_array($file, config('backpack.langfilemanager.language_ignore'))) {
            abort('403', trans('backpack::langfilemanager.cant_edit_online'));
        }

        $message = trans('error.error_general');
        $status = false;

        if ($lang) {
            $langfile->setLanguage($lang);
        }

        $langfile->setFile($file);


            if ($langfile->setFileContent($request->all())) {
                \Alert::success(trans('backpack::langfilemanager.saved'))->flash();
                $status = true;
            }


        return redirect()->back();
    }
        else{
            return abort(401, 'You are not authorized to access this resource.');
        }
    }



    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        if (\Auth::user()->can('translate assets') || \Auth::user()->hasRole('admin')) {
        $this->crud->hasAccessOrFail('show');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;
        $this->data['title'] = trans('backpack::crud.preview').' '.$this->crud->entity_name;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view('crud::show', $this->data);
    }
        else{
            return abort(401, 'You are not authorized to access this resource.');
        }
    }
    /**
     * get the content of a language file as an array
     * @return	array|false
     */
    public function getFileContent($lang,$file)
    {
        $filepath = $this->getFilePath($lang,$file);

        if (is_file($filepath)) {
            $wordsArray = include $filepath;


            return $wordsArray;
        }

        return false;
    }

    private function getFilePath($lang,$file)
    {
        return base_path("resources/lang/{$lang}/{$file}.php");
    }


}
