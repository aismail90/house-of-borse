<?php

namespace app\http\controllers\Admin\Backpack;

// use Illuminate\Support\Facades\Request;

class AdminController extends \Backpack\Base\app\Http\Controllers\AdminController
{



    /**
     * Redirect to the dashboard.
     *
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function redirect()
    {

        // The '/admin' route is not to be used as a page, because it breaks the menu's active state.
        return redirect(\App::getLocale().'/admin/dashboard');
    }
}
