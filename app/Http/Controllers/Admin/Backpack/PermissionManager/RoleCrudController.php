<?php

namespace App\Http\Controllers\Admin\Backpack\PermissionManager ;

// use Backpack\CRUD\app\Http\Controllers\CrudController;
// VALIDATION
use Backpack\PermissionManager\app\Http\Requests\RoleCrudRequest as StoreRequest;
use Backpack\PermissionManager\app\Http\Requests\RoleCrudRequest as UpdateRequest;

class RoleCrudController extends \Backpack\PermissionManager\app\Http\Controllers\RoleCrudController
{
    /**
     * Update the specified resource in the database.
     *
     * @param UpdateRequest $request - type injection used for validation using Requests
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request = null)
    {
        if (\Auth::user()->can('manage users')|| \Auth::user()->hasRole('admin')) {
            $this->crud->hasAccessOrFail('update');

            // fallback to global request instance
            if (is_null($request)) {
                $request = \Request::instance();
            }

            // replace empty values with NULL, so that it will work with MySQL strict mode on
            foreach ($request->input() as $key => $value) {
                if (empty($value) && $value !== '0') {
                    $request->request->set($key, null);
                }
            }

            // update the row in the db
            $item = $this->crud->update($request->get($this->crud->model->getKeyName()),
                $request->except('redirect_after_save', '_token'));
            $this->data['entry'] = $this->crud->entry = $item;

            // show a success message
            \Alert::success(trans('backpack::crud.update_success'))->flash();

            return \Redirect::to($request->segment(1) . '/' . $this->crud->route);
        } else {
            return abort(401, 'You are not authorized to access this resource.');
        }
    }


    /**
     * Store a newly created resource in the database.
     *
     * @param StoreRequest $request - type injection used for validation using Requests
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    function store(StoreRequest $request = null)
    {
        if (\Auth::user()->can('manage users')|| \Auth::user()->hasRole('admin')) {
            $this->crud->hasAccessOrFail('create');

            // fallback to global request instance
            if (is_null($request)) {
                $request = \Request::instance();
            }

            // replace empty values with NULL, so that it will work with MySQL strict mode on
            foreach ($request->input() as $key => $value) {
                if (empty($value) && $value !== '0') {
                    $request->request->set($key, null);
                }
            }

            $this->validate($request, [
                'name' => 'required|unique:roles,name',
            ]);

            // insert item in the db
            $item = $this->crud->create($request->except(['redirect_after_save', '_token']));
            $this->data['entry'] = $this->crud->entry = $item;

            // show a success message
            \Alert::success(trans('backpack::crud.insert_success'))->flash();

            // redirect the user where he chose to be redirected
            switch ($request->input('redirect_after_save')) {
                case 'current_item_edit':
                    return \Redirect::to($request->segment(1) . '/' . $this->crud->route . '/' . $item->getKey() . '/edit');

                default:
                    return \Redirect::to($request->input('redirect_after_save'));
            }
        } else {
            return abort(401, 'You are not authorized to access this resource.');
        }
    }


    /**
     * Display all rows in the database for this entity.
     *
     * @return Response
     */
    public function index()
    {
        if(\Auth::user()->can('manage users')|| \Auth::user()->hasRole('admin')){
            $this->crud->hasAccessOrFail('list');

            $this->data['crud'] = $this->crud;
            $this->data['title'] = ucfirst($this->crud->entity_name_plural);

            // get all entries if AJAX is not enabled
            if (! $this->data['crud']->ajaxTable()) {
                $this->data['entries'] = $this->data['crud']->getEntries();
            }

            // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
            // $this->crud->getListView() returns 'list' by default, or 'list_ajax' if ajax was enabled
            return view('crud::list', $this->data);
        }
        else{
            return abort(401, 'You are not authorized to access this resource.');
        }
    }

    /**
     * Show the form for creating inserting a new row.
     *
     * @return Response
     */
    public function create()
    {
        if(\Auth::user()->can('manage users') || \Auth::user()->hasRole('admin') ){
            $this->crud->hasAccessOrFail('create');

            // prepare the fields you need to show
            $this->data['crud'] = $this->crud;
            $this->data['fields'] = $this->crud->getCreateFields();
            $this->data['title'] = trans('backpack::crud.add').' '.$this->crud->entity_name;

            // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
            return view('crud::create', $this->data);
        }
        else{
            return abort(401, 'You are not authorized to access this resource.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        if($id==1 ){
            abort('404');
        }
        else{
            if(\Auth::user()->can('manage users')|| \Auth::user()->hasRole('admin')){
                $this->crud->hasAccessOrFail('update');
                // get the info for that entry
                $this->data['entry'] = $this->crud->getEntry($id);
                $this->data['crud'] = $this->crud;
                $this->data['fields'] = $this->crud->getUpdateFields($id);
                $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;

                $this->data['id'] = $id;

                // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
                return view('crud::edit', $this->data);
            }
            else{
                return abort(401, 'You are not authorized to access this resource.');
            }

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return string
     */
    public function destroy($id)
    {
        if($id==1 ){
            abort('404');
        }
        else{
            if(\Auth::user()->can('manage users')|| \Auth::user()->hasRole('admin')){
                $this->crud->hasAccessOrFail('delete');

                return $this->crud->delete($id);
            }
            else{
                return abort(401, 'You are not authorized to access this resource.');
            }
        }

    }


}
