<?php

namespace App\Http\Controllers\Admin\Backpack\PermissionManager ;


use App\Http\Requests;
// use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\PermissionManager\app\Http\Requests\UserStoreCrudRequest as StoreRequest;
// VALIDATION
use Backpack\PermissionManager\app\Http\Requests\UserUpdateCrudRequest as UpdateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;


class UserCrudController extends \Backpack\PermissionManager\app\Http\Controllers\UserCrudController
{
    /**
     * Update the specified resource in the database.
     *
     * @param UpdateRequest $request - type injection used for validation using Requests
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request)
    {
        if (\Auth::user()->can('manage users') || \Auth::user()->hasRole('admin')) {
        //encrypt password and set it to request
        $this->crud->hasAccessOrFail('update');

        $dataToUpdate = \Request::except(['redirect_after_save', 'password']);

        //encrypt password
        if ($request->input('password')) {
            $dataToUpdate['password'] = bcrypt($request->input('password'));
        }

        // update the row in the db
        $this->crud->update(\Request::get('id'), $dataToUpdate);

        // show a success message
        \Alert::success(trans('backpack::crud.update_success'))->flash();

        return \Redirect::to($request->segment(1).'/'.$this->crud->route);
        }
        else {
            return abort(401, 'You are not authorized to access this resource.');
        }
    }




    /**
     * Store a newly created resource in the database.
     *
     * @param StoreRequest $request - type injection used for validation using Requests
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        if (\Auth::user()->can('manage users') || \Auth::user()->hasRole('admin') ) {
        $this->crud->hasAccessOrFail('create');

        // insert item in the db
        if ($request->input('password')) {
            $item = $this->crud->create(\Request::except(['redirect_after_save']));

            // now bcrypt the password
            $item->password = bcrypt($request->input('password'));
            $item->save();
        } else {
            $item = $this->crud->create(\Request::except(['redirect_after_save', 'password']));
        }
        $this->validate($request, [
            'password' => 'required|min:6|confirmed',

        ]);

        // show a success message
        \Alert::success(trans('backpack::crud.insert_success'))->flash();

        // redirect the user where he chose to be redirected
        switch (\Request::input('redirect_after_save')) {
            case 'current_item_edit':
                return \Redirect::to($request->segment(1).'/'.$this->crud->route.'/'.$item->id.'/edit');

            default:
                return \Redirect::to(\Request::input('redirect_after_save'));
        }
        }
        else {
            return abort(401, 'You are not authorized to access this resource.');
            }
    }


    /**
     * Display all rows in the database for this entity.
     *
     * @return Response
     */
    public function index()
    {
        if(\Auth::user()->can('manage users')|| \Auth::user()->hasRole('admin')){
            $this->crud->hasAccessOrFail('list');

            $this->data['crud'] = $this->crud;
            $this->data['title'] = ucfirst($this->crud->entity_name_plural);

            // get all entries if AJAX is not enabled
            if (! $this->data['crud']->ajaxTable()) {
                $this->data['entries'] = $this->data['crud']->getEntries();
            }

            // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
            // $this->crud->getListView() returns 'list' by default, or 'list_ajax' if ajax was enabled
            return view('crud::list', $this->data);
        }
        else{
            return abort(401, 'You are not authorized to access this resource.');
        }
    }

    /**
     * Show the form for creating inserting a new row.
     *
     * @return Response
     */
    public function create()
    {
        if(\Auth::user()->can('manage users')|| \Auth::user()->hasRole('admin')){
            $this->crud->hasAccessOrFail('create');

            // prepare the fields you need to show
            $this->data['crud'] = $this->crud;
            $this->data['fields'] = $this->crud->getCreateFields();
            $this->data['title'] = trans('backpack::crud.add').' '.$this->crud->entity_name;

            // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
            return view('crud::create', $this->data);
        }
        else{
            return abort(401, 'You are not authorized to access this resource.');
        }
    }

    public function showBasicInfoForm(){

        return View('user/edit_user_info');
    }
    public function editBasicInfo(UpdateRequest $request){

        Auth::user()->name=$request->input('name');
        Auth::user()->email=$request->input('email');
        if ($request->input('password')) {
            $bcryptPassword = bcrypt($request->input('password'));
            Auth::user()->password=$bcryptPassword;
            $this->validate($request, [
                'password' => 'required|min:6|confirmed',

            ]);
        }
        Auth::user()->save();
        return redirect(route('editBasicInfo'));


    }




}
