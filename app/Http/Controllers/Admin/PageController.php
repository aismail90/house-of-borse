<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\PageRequest;
use App\Enum\GeneralEnum;

class PageController extends \App\Http\Controllers\Controller
{
	function __construct()
	{
		parent::__construct();
	}
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	if(!(\Auth::user()->hasRole('admin') || \Auth::user()->can('view pages'))) {

    		$data = App('PageManager')->getPages(config('app.fallback_locale'), \Auth::user()->id);
			if(!$data) {
				return View('errors/403', $this->data);
			}
    	} else {
    		$data = App('PageManager')->getPages(config('app.fallback_locale'));
    	}
    	
    	$this->data['pages'] = $data;
    	
    	$this->data['languages'] 		= App('PageManager')->getLanguages();
    	$this->data['default_locale']	= config('app.fallback_locale');
    	
    	$this->data['title'] = trans('admin.list_pages');
    	
    	return View('pages/index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	if(!(\Auth::user()->hasRole('admin') || \Auth::user()->can('add page'))) {
    		return View('errors/403', $this->data);
    	}
    	
    	$this->data['page'] = NULL;
    		
    	$this->data['languages'] 	= App('PageManager')->getLanguages(config('app.fallback_locale'));
    	$this->data['page_status'] 	= App('PageManager')->getPageStatus();
    	$this->data['parent_pages'] = App('PageManager')->getParentPages();
    	$this->data['page_types'] 	= App('PageManager')->getPageTypes(); 
    	
    	$this->data['page_locale'] = config('app.fallback_locale');
    	
    	$this->data['title'] = trans('admin.add_page');
    	
    	return View('pages/create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PageRequest $request)
    {
    	$inputs = $request->all();
    	
    	$page = App('PageManager')->savePage($inputs);
    	if($page) {
    		
    		$request->session()->flash('message', 'Page successfully saved!');
    		return redirect()->route('page.edit', ['id' => $page['id'], 'from' => $page['lang']]);
    		
    	} else {
    		echo 'error!';
    	}
    }

    /**
     * Display the specified resource.
     *
     * @param  varchar  $page_slug
     * @return \Illuminate\Http\Response
     */
    public function show($page_slug, $preview = FALSE)
    {    	
    	if($preview && $preview !== 'preview') {
    		abort(404);
    	}
    	
    	$data = App('PageManager')->getPageDetails($this->locale, 'slug', $page_slug, $preview);
    	if($data) {
    		$this->data['page'] = $data;
    		
    		$this->data['body_class'] = 'institutional-template';
    		
    		if($page_slug === 'institutional-services') {
    			$this->data['body_class'] = 'institutional-template';
    		} elseif ($page_slug === 'forex' || $page_slug === 'metals') {
    			$this->data['body_class'] = 'forex-template';
    		} elseif ($page_slug === 'about-hob') {
    			$this->data['body_class'] = 'about-template';
    		} 
    		
    		$this->data['page_slug'] = $page_slug;
    		
    		return View('pages/show', $this->data);
    	} else {
    		abort(404);
    	}
    	
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
    	$page_type = GeneralEnum::$page_type['edit'];
    	$page_perm = \Auth::user()->can('edit page');
    	
    	$locale = config('app.fallback_locale');
    	if(!empty($request->get('from'))) {
    		$locale = $request->get('from');
    	}
    	if(!empty($request->get('to'))) {
    		$this->data['translate_to'] = $request->get('to');
    		$page_perm = \Auth::user()->can('translate page');
    	}
    	$this->data['page_locale'] = $locale;
    	
    	$data = App('PageManager')->getPageDetails($locale, 'id', (int) $id);
    	if($data) {
    		
    		if(!(\Auth::user()->hasRole('admin') || $page_perm)) {
    			if(\Auth::user()->id != $data['created_by'] && $page_type == GeneralEnum::$page_type['edit']) {
    				return View('errors/403', $this->data);
    			}
    		}

    		$this->data['page'] = $data;
    		
    		$this->data['page_translated_languages'] = App('PageManager')->selectLanguagesOfPage($id, TRUE);
    		
	    	if(!empty($request->get('to'))) {
	    		$this->data['languages'] = App('PageManager')->getLanguages(FALSE, array_keys($this->data['page_translated_languages']));
	    	} else {
	    		$this->data['languages'] = App('PageManager')->selectLanguagesOfPage($id, TRUE);
	    	}
    		
    		
    		$this->data['page_status'] 	= App('PageManager')->getPageStatus();
    		$this->data['parent_pages'] = App('PageManager')->getParentPages();
    		$this->data['page_types'] 	= App('PageManager')->getPageTypes();
    		
    		$this->data['title'] = trans('admin.edit_page');
    		
    		return View('pages/edit', $this->data);
    	} else {
    		abort(404);
    	}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PageRequest $request, $id)
    {
    	$inputs = $request->all();
    	
//     	$page = App('PageManager')->updatePage($inputs, $this->locale, $id);
    	$page = App('PageManager')->savePage($inputs);
    	if($page) {
    		
    		$request->session()->flash('message', 'Page successfully updated!');
    		return redirect()->route('page.edit', ['id' => $id, 'from' => $page['lang']]);
    		
    	} else {
    		echo 'error!';
    	}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function ajaxCreatePageSlug(Request $request)
	{
    	$response = [];
    	if($request->ajax() && $request->isMethod('get')) {
    		
    		$title = $request->get('title');
    		$locale = config('app.fallback_locale');
    		if(!empty($request->get('locale'))) {
    			$locale = $request->get('locale');
    		}
    		
    		$slug = App('PageManager')->createPageSlug($title);
    		if($slug) {
    			$response['code'] 	= 200;
    			$response['slug'] 	= $slug;
//     			$response['href'] 	= route('page.show', ['page_slug' => $slug]);
//     			$response['link'] 	= route('page.show', ['page_slug' => '']);
    			$response['href'] 	= url('/'.$locale, [$slug]);
    			$response['link'] 	= url('/'.$locale, ['']);
    			
    		} else {
    			$response['code'] = 404;
    		}
    	} else {
    		$response['code'] = 404;
    	}
    	return response()->json($response);
	}
	
	public function ajaxCheckIfSlugExists(Request $request)
	{
		$response = [];
		if($request->ajax() && $request->isMethod('get')) {
		
			$slug = $request->get('slug');
			$page_id = FALSE;
			if($request->get('page_id')) {
				$page_id = $request->get('page_id');
			}
			$locale = config('app.fallback_locale');
			if(!empty($request->get('locale'))) {
				$locale = $request->get('locale');
			}
			$slug = App('PageManager')->checkIfSlugExists($slug, $page_id);
			if($slug) {
				$response['code'] 	= 200;
				$response['slug'] 	= $slug;
// 				$response['href'] 	= route('page.show', ['page_slug' => $slug]);
// 				$response['link'] 	= route('page.show', ['page_slug' => '']);
				$response['href'] 	= url('/'.$locale, [$slug]);
				$response['link'] 	= url('/'.$locale, ['']);
			} else {
				$response['code'] = 404;
			}
		} else {
			$response['code'] = 404;
		}
		return response()->json($response);
	}
	
	public function ajaxCreatePreviewPage(Request $request)
	{
		$response = [];
		if($request->ajax() && $request->isMethod('post')) {
		
			$inputs = $request->all();
	    	
	    	$page_id = App('PageManager')->savePage($inputs, TRUE);
	    	if($page_id) {
	    		$response['code'] = 200;
	    		$response['page_id'] = $page_id;
	    	} else {
	    		$response['code'] = 404;
	    	}
			
		} else {
			$response['code'] = 404;
		}
		return response()->json($response);
	}
}
