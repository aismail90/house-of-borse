<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\PairRequest;
use App\Enum\GeneralEnum;

class PairController extends \App\Http\Controllers\Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {    	
    	if(!(\Auth::user()->hasRole('admin') || \Auth::user()->can('view pairs'))) {
    		return View('errors/403', $this->data);
    	}
    	
    	$data = App('PairManager')->getPairs();
    	 
    	$this->data['pairs'] = $data;
    	 
    	$this->data['title'] = trans('admin.list_pairs');
    	 
    	return View('pairs/index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	if(!(\Auth::user()->hasRole('admin') || \Auth::user()->can('add pair'))) {
    		return View('errors/403', $this->data);
    	}
    	
    	$this->data['pair'] = NULL;
    	
    	$this->data['page_types'] 	= App('PageManager')->getPageTypes();
    	$this->data['pair_types'] 	= App('PairManager')->getPairTypes();
    	
    	$this->data['title'] = trans('admin.add_pair');
    	
    	return View('pairs/create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PairRequest $request)
    {
    	$inputs = $request->all();
    	
    	$pair = App('PairManager')->savePair($inputs);
    	if($pair) {
    		
    		$request->session()->flash('message', 'Pair successfully saved!');
    		return redirect()->route('pair.edit', ['id' => $pair['id']]);
    		
    	} else {
    		echo 'error!';
    	}
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
    	if(!(\Auth::user()->hasRole('admin') || \Auth::user()->can('edit pair'))) {
    		return View('errors/403', $this->data);
    	}
    	
    	$page_type = GeneralEnum::$page_type['edit'];
    	
    	$data = App('PairManager')->getPairDetails($id);
    	if($data) {
    	
    		$this->data['pair'] = $data;
    	
    		$this->data['page_types'] 	= App('PageManager')->getPageTypes();
    		$this->data['pair_types'] 	= App('PairManager')->getPairTypes();
    	
    		$this->data['title'] = trans('admin.edit_pair');
    	
    		return View('pairs/edit', $this->data);
    	} else {
    		abort(404);
    	}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PairRequest $request, $id)
    {
    	$inputs = $request->all();
    	
    	$pair = App('PairManager')->savePair($inputs);
    	if($pair) {
    		
    		$request->session()->flash('message', 'Pair successfully updated!');
    		return redirect()->route('pair.edit', ['id' => $id]);
    		
    	} else {
    		echo 'error!';
    	}
    }
    
    public function ajaxQuickUpdatePair(Request $request)
    {
    	$response = [];
    	if($request->ajax() && $request->isMethod('get')) {
    	
    		$inputs = $request->all();
    	
    		$pair = App('PairManager')->savePair($inputs);
    		if($pair) {
    			$response['code'] = 200;
    		} else {
    			$response['code'] = 404;
    		}
    			
    	} else {
    		$response['code'] = 404;
    	}
    	return response()->json($response);
    }
}
