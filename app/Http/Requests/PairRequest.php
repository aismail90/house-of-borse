<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Enum\GeneralEnum;

class PairRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		if($this->get('form_type') == GeneralEnum::$page_type['edit']) {
			return [
				'pair' 		=> 'unique:pairs,pair,'.$this->get('pair_id'),
				'type'		=> 'required',
				'im_factor'			=> 'required',
				'trading_hours'		=> 'required',
				'tick_factor'		=> 'required',
			];
		}
		return [
			'pair' 				=> 'required|unique:pairs,pair',
			'type'				=> 'required',
			'im_factor'			=> 'required',
			'trading_hours'		=> 'required',
			'tick_factor'		=> 'required',
		];
	}
}
