<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PageRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		if($this->get('preview') !== NULL) {
			return [];
		}
		return [
			'title' 		=> 'required',
// 			'content' 		=> 'required',
			'status' 		=> 'required',
			'language' 		=> 'required',
			'parent' 		=> 'required',
		];
	}
}
