<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $language=config('app.fallback_locale');
        if($request->segment(1)){
            $language=$request->segment(1);
        }
        if (Auth::guard($guard)->check()) {
            return redirect(url($language.'/admin/dashboard'));
        }

        return $next($request);
    }
}
