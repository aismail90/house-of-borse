<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 3/15/17
 * Time: 4:59 PM
 */

namespace App\Providers;


use App\Managers\CountryManager;
use Illuminate\Support\ServiceProvider;

class CountryManagerServiceProvider extends ServiceProvider {
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->singleton(CountryManager::class, function($app) {
            return new CountryManager();
        });

    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [CountryManager::class];
    }
} 