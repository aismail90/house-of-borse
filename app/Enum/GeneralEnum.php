<?php 

namespace App\Enum;

class GeneralEnum 
{ 
	public static $languages = ['en' => 'english', 'ar' => 'arabic'];
	
	public static $page_status = [1 => 'publish', 2 => 'draft', 3 => 'preview'];
	
	public static $parent_pages = [
		1 => ['trans' => 'menu_trading_products'],
		2 => ['trans' => 'menu_trading_accounts'],
		3 => ['trans' => 'menu_support'],
		4 => ['trans' => 'menu_platforms'],
		5 => ['trans' => 'menu_partners'],
		6 => ['trans' => 'menu_about_us'],
	];
	
	public static $page_preview = ['true' => 1, 'false' => 0];
	
	public static $page_type = ['add' => 1, 'edit' => 2, 'translate' => 3]; 
	
	public static $spread_symbols = ['NZDUSD', 'USDCHF', 'GBPUSD', 'USDJPY', 'USDCAD'];
	
	public static $pair_type = [
		1 => 'common',	
		2 => 'minors',
		3 => 'exotic'
	];
}

?>
