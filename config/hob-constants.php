<?php 

define('PROJECT_ROOT_PATH', '/opt/lampp/htdocs');

define('PROJECT_FOLDER_PATH', '/last-portal');
define('PROJECT_PUBLIC_PATH', '/public');
define('PROJECT_UPLOAD_PATH', '/uploads');

define('HOB_PATH', PROJECT_ROOT_PATH.PROJECT_FOLDER_PATH);

define('HOB_PUBLIC_PATH', HOB_PATH.'/public');

define('HOB_JSON_PATH', HOB_PUBLIC_PATH.'/json');

