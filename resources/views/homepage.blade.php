@extends('layouts.default_layout')

@section('after_scripts')
	{{ Html::script('js/socket.io-1.4.5.js') }}
	{{ Html::script('js/homepage/homepage.js') }}
@endsection

@section('content')
	
	@include('elements.homepage.create_accounts')
	@include('elements.homepage.spread')
	@include('elements.homepage.why_hob')
	@include('elements.homepage.trading_platforms')
	@include('elements.homepage.our_products')
	
@endsection


	