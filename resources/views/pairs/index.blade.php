@extends('backpack::layout')

@section('after_scripts')
	{{ Html::script('js/admin/pairs/listing.js') }}
@endsection

@section('header')
	<section class="content-header">
	  <h1>
	    <span class="text-capitalize">{{ trans('admin.pairs') }}</span>
	  </h1>
	  <ol class="breadcrumb">
	    <li><a href="{{ url($locale.'/admin/dashboard') }}">{{ trans('admin.admin') }}</a></li>
	    <li><a href="{{ route('pair.index') }}" class="text-capitalize">{{ trans('admin.pairs') }}</a></li>
	    <li class="active">{{ trans('admin.list') }}</li>
	  </ol>
	</section>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				
				@if (Auth::user()->hasRole('admin') || Auth::user()->can('add pair'))
					<div class="box-header with-border">
	          			<a href="{{ route('pair.create') }}" class="btn btn-primary ladda-button" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-plus"></i> {{ trans('admin.add_pair') }} </span></a>	  		  			  		
	          			<div id="datatable_button_stack" class="pull-right text-right"></div>
	        		</div>
				@endif
						
        		<div class="box-body">
        			<div id="crudTable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
        				<div class="row">
        					<div class="col-sm-12">
        						<table id="crudTable" class="table table-bordered table-striped display dataTable" role="grid" aria-describedby="crudTable_info">
        							<thead>
        								<tr role="row">
        									<th>{{ trans('admin.pair') }}</th>
        									<th>{{ trans('admin.pair_type') }}</th>
        									<th>{{ trans('admin.min_spread') }}</th>
        									<th>{{ trans('admin.avg_spread') }}</th>
        									<th>{{ trans('admin.im_factor') }}</th>
        									<th>{{ trans('admin.trading_hours') }}</th>
        									<th>{{ trans('admin.value_date') }}</th>
        									<th>{{ trans('admin.tick_factor') }}</th>
        									<th>{{ trans('admin.min_trade_size') }}</th>
        									<th>{{ trans('admin.contract_size_lot') }}</th>
        									<th>{{ trans('admin.actions') }}</th>
        								</tr>
        							</thead>
        							<tbody>
        								@if (!empty($pairs))
        									<?php $i = 0; ?>
										    @foreach ($pairs as $id => $pair)
											    <?php 
											    	$i++;
											    	$tr_class = 'odd';
											    ?>
											    @if($i % 2 == 0)
											    	<?php $tr_class = 'even'; ?>
											    @endif
											    <tr data-entry-id="{{ $id }}" role="row" class="{{ $tr_class }}">
													<td id="td_txt_pair_{{ $id }}">{{ $pair['pair'] }}</td>
													<td id="td_txt_type_{{ $id }}" type="{{ $pair['type'] }}">{{ $pair['type_lbl'] }}</td>                    
													<td class="td_txt_{{ $id }}" id="td_txt_min_spread_{{ $id }}">{{ $pair['min_spread'] }}</td>
													<td class="td_npt_{{ $id }} hidden" id="td_npt_min_spread_{{ $id }}"><input type="text" value="{{ $pair['min_spread'] }}" id="npt_min_spread_{{ $id }}" style="width: 80px;"></td>                                
													<td class="td_txt_{{ $id }}" id="td_txt_avg_spread_{{ $id }}">{{ $pair['avg_spread'] }}</td>
													<td class="td_npt_{{ $id }} hidden" id="td_npt_avg_spread_{{ $id }}"><input type="text" value="{{ $pair['avg_spread'] }}" id="npt_avg_spread_{{ $id }}" style="width: 80px;"></td>
													<td class="td_txt_{{ $id }}" id="td_txt_im_factor_{{ $id }}">{{ $pair['im_factor'] }}</td>
													<td class="td_npt_{{ $id }} hidden" id="td_npt_im_factor_{{ $id }}"><input type="text" value="{{ $pair['im_factor'] }}" id="npt_im_factor_{{ $id }}" style="width: 80px;"></td>
													<td class="td_txt_{{ $id }}" id="td_txt_trading_hours_{{ $id }}"><span id="td_span_trading_hours_{{ $id }}">{{ $pair['trading_hours'] }}</span> {{ trans('admin.hr') }}</td>
													<td class="td_npt_{{ $id }} hidden" id="td_npt_trading_hours_{{ $id }}"><input type="text" value="{{ $pair['trading_hours'] }}" id="npt_trading_hours_{{ $id }}" style="width: 80px;"> {{ trans('admin.hr') }}</td>
													<td class="td_txt_{{ $id }}" id="td_txt_value_date_{{ $id }}">{{ trans('admin.gmt') }} <span id="td_span_value_date_{{ $id }}">{{ $pair['value_date'] }}</span></td>
													<td class="td_npt_{{ $id }} hidden" id="td_npt_value_date_{{ $id }}">{{ trans('admin.gmt') }} <input type="text" value="{{ $pair['value_date'] }}" id="npt_value_date_{{ $id }}" style="width: 80px;"></td>
													<td class="td_txt_{{ $id }}" id="td_txt_tick_factor_{{ $id }}">{{ $pair['tick_factor'] }}</td>
													<td class="td_npt_{{ $id }} hidden" id="td_npt_tick_factor_{{ $id }}"><input type="text" value="{{ $pair['tick_factor'] }}" id="npt_tick_factor_{{ $id }}" style="width: 80px;"></td>
													<td class="td_txt_{{ $id }}" id="td_txt_min_trade_size_{{ $id }}">{{ $pair['min_trade_size'] }}</td>
													<td class="td_npt_{{ $id }} hidden" id="td_npt_min_trade_size_{{ $id }}"><input type="text" value="{{ $pair['min_trade_size'] }}" id="npt_min_trade_size_{{ $id }}" style="width: 80px;"></td>
													<td class="td_txt_{{ $id }}" id="td_txt_contract_size_lot_{{ $id }}">{{ $pair['contract_size_lot'] }}</td>
													<td class="td_npt_{{ $id }} hidden" id="td_npt_contract_size_lot_{{ $id }}"><input type="text" value="{{ $pair['contract_size_lot'] }}" id="npt_contract_size_lot_{{ $id }}" style="width: 80px;"></td>
													<td>	
														@if (Auth::user()->hasRole('admin') || Auth::user()->can('edit pair'))
			                                      			<a href="{{ route('pair.edit', ['id' => $id]) }}" class="btn btn-xs btn-default"><i class="fa fa-edit"></i> {{ trans('admin.edit') }} </a>
			                                      			<a href="javascript:void(0);" class="btn btn-xs btn-default btn_quick_edit" pair_id="{{ $id }}"><i class="fa fa-edit"></i> {{ trans('admin.quick_edit') }} </a>
			                                      			<a href="javascript:void(0);" class="btn btn-xs btn-default hidden btn_save" pair_id="{{ $id }}"><i class="fa fa-save"></i> {{ trans('admin.save') }} </a>
			                                      			<a href="javascript:void(0);" class="btn btn-xs btn-default hidden btn_cancel" pair_id="{{ $id }}"><i class="fa fa-cancel"></i> {{ trans('admin.cancel') }} </a>
			  	                    					@endif
			  	                    				</td>	
		                						</tr>
											@endforeach
        								@else
        									<tr class="odd"><td valign="top" colspan="4" class="dataTables_empty">{{ trans('admin.no_data_in_tbl') }}</td></tr>
        								@endif
        							</tbody>
        						</table>
        					</div>
        				</div>
        			</div>
        		</div>
			</div>
		</div>
	</div>
@endsection

<input type="hidden" id="ajaxUpdatePairs" value="{{ route('pair.quick_update') }}">