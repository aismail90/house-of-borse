@extends('backpack::layout')

@section('after_scripts')
	{{ Html::script('js/parsely/parsley.min.js') }}
	{{ Html::script('js/parsely/i18n/en.js') }}
	{{ Html::script('js/admin/pairs/pair_form.js') }}
	{{ Html::script('js/parsely/parsely_custom_validations.js') }}
@endsection

@section('header')
	<section class="content-header">
	  <h1>
	  	{{ trans('admin.add_pair') }}
	  </h1>
	  <ol class="breadcrumb">
	    <li><a href="{{ url($locale.'/admin/dashboard') }}">{{ trans('admin.admin') }}</a></li>
	    <li><a href="{{ route('pair.index') }}" class="text-capitalize">{{ trans('admin.pairs') }}</a></li>
	    <li class="active">{{ trans('admin.add') }}</li>
	  </ol>
	</section>
@endsection


@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <a href="{{ route('pair.index') }}"><i class="fa fa-angle-double-left"></i> {{ trans('admin.back_all_pairs') }} </a>
            <br><br>
            
            @include('elements.pairs.form', [
            	'form_data' => ['route' => ['pair.store'], 'method'=> 'POST', 'id' => 'pair_form'], 
            	'form_type' => $page_types['add'], 
            	'box_title' => trans('admin.add_pair')]
            )
            
        </div>
    </div>
@endsection


	