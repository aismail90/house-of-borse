@extends('backpack::layout')

@section('after_scripts')
	{{ Html::script('js/parsely/parsley.min.js') }}
	{{ Html::script('js/parsely/i18n/en.js') }}
	{{ Html::script('js/admin/pairs/pair_form.js') }}
	{{ Html::script('js/parsely/parsely_custom_validations.js') }}
@endsection

@section('header')
	<section class="content-header">
	  <h1>
	  	{{ trans('admin.edit_pair') }}
	  </h1>
	  <ol class="breadcrumb">
	    <li><a href="{{ url($locale.'/admin/dashboard') }}">{{ trans('admin.admin') }}</a></li>
	    <li><a href="{{ route('pair.index') }}" class="text-capitalize">{{ trans('admin.pairs') }}</a></li>
	    <li class="active">{{ trans('admin.edit') }}</li>
	  </ol>
	</section>
@endsection


@section('content')
	<div class="row">
        <div class="col-md-8 col-md-offset-2">
            <a href="{{ route('pair.index') }}"><i class="fa fa-angle-double-left"></i> {{ trans('admin.back_all_pairs') }} </a>
            <br><br>
            
            <?php 
            	$form_type = $page_types['edit'];
            	$box_title = trans('admin.edit_page');
            ?>
            
            @include('elements.pairs.form', [
            	'form_data' 	=> ['route' => ['pair.update', $pair['id']], 'method'=> 'POST', 'id' => 'pair_form'], 
            	'form_type' 	=> $form_type, 
            	'box_title' 	=> $box_title
            ])
            
        </div>
    </div>
@endsection


	