@extends('backpack::layout')

@section('header')
	<section class="content-header">
	  <h1>
	    <span class="text-capitalize">{{ trans('admin.pages') }}</span>
	  </h1>
	  <ol class="breadcrumb">
	    <li><a href="{{ url($locale.'/admin/dashboard') }}">{{ trans('admin.admin') }}</a></li>
	    <li><a href="{{ route('page.index') }}" class="text-capitalize">{{ trans('admin.pages') }}</a></li>
	    <li class="active">{{ trans('admin.list') }}</li>
	  </ol>
	</section>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box">
			
				@if (Auth::user()->hasRole('admin') || Auth::user()->can('add page'))
					<div class="box-header with-border">
	          			<a href="{{ route('page.create') }}" class="btn btn-primary ladda-button" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-plus"></i> {{ trans('admin.add_page') }} </span></a>	  		  			  		
	          			<div id="datatable_button_stack" class="pull-right text-right"></div>
	        		</div>
				@endif
			
        		<div class="box-body">
        			<div id="crudTable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
        				<div class="row">
        					<div class="col-sm-12">
        						<table id="crudTable" class="table table-bordered table-striped display dataTable" role="grid" aria-describedby="crudTable_info">
        							<thead>
        								<tr role="row">
        									<th class="sorting_asc" tabindex="0" aria-controls="crudTable" rowspan="1" colspan="1" style="width: 209px;" aria-sort="ascending" aria-label="Name: activate to sort column descending">{{ trans('admin.title') }}</th>
        									<th class="sorting" tabindex="0" aria-controls="crudTable" rowspan="1" colspan="1" style="width: 282px;" aria-label="Template: activate to sort column ascending">{{ trans('admin.parent') }}</th>
        									<th class="sorting" tabindex="0" aria-controls="crudTable" rowspan="1" colspan="1" style="width: 389px;" aria-label="Slug: activate to sort column ascending">{{ trans('admin.status') }}</th>
        									<th class="sorting" tabindex="0" aria-controls="crudTable" rowspan="1" colspan="1" style="width: 554px;" aria-label="Actions: activate to sort column ascending">{{ trans('admin.page_languages') }}</th>
        									<th class="sorting" tabindex="0" aria-controls="crudTable" rowspan="1" colspan="1" style="width: 554px;" aria-label="Actions: activate to sort column ascending">{{ trans('admin.actions') }}</th>
        								</tr>
        							</thead>
        							<tbody>
        								@if (!empty($pages))
        									<?php $i = 0; ?>
										    @foreach ($pages as $id => $page)
											    <?php 
											    	$i++;
											    	$tr_class = 'odd';
											    ?>
											    @if($i % 2 == 0)
											    	<?php $tr_class = 'even'; ?>
											    @endif
											    <?php $is_owner = FALSE; ?>
											    @if (Auth::user()->id == $page['created_by'])
											    	<?php $is_owner = TRUE; ?> 
											    @endif
											    <?php $need_translation = TRUE; ?>
											    @if (count($languages) == count($page['lang']))
											    	<?php $need_translation = FALSE; ?>
											    @endif
											    <tr data-entry-id="{{ $id }}" role="row" class="{{ $tr_class }}">
													<td class="sorting_1">{{ $page['title'] }}</td>                    
													<td>{{ $page['parent_lbl'] }}</td>                                                                  
													<td>{{ $page['status_lbl'] }}</td>
													
													<td><?php echo implode(', ', $page['lang']); ?></td>             
													                    
		                                      		<td>
		                                      			@if($page['status'] == 1)
		                      								<a class="btn btn-default btn-xs" href="{{ route('page.show', ['slug' => $page['slug']]) }}" target="_blank"><i class="fa fa-eye"></i> {{ trans('admin.open') }} </a>
		                      							@endif
<!-- 			  		  			  		  				<a href="{{ route('page.edit', ['id' => $id]) }}" class="btn btn-xs btn-default"><i class="fa fa-edit"></i> {{ trans('admin.edit') }} </a> -->
			  		  			  		  				
			  		  			  		  				@if (Auth::user()->hasRole('admin') || Auth::user()->can('edit page') || $is_owner || (Auth::user()->can('translate page') && $need_translation))
			  		  			  		  					<div class="btn-group">
																<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
															    	 <i class="fa fa-edit"></i> {{ trans('admin.edit') }} <span class="caret"></span>
															  	</button>
															  	<ul class="dropdown-menu">
															  		@if ($languages)
															  			@foreach ($languages as $key => $val)
															  				@if(in_array($key, $page['lang']))
															  					@if(Auth::user()->hasRole('admin') || Auth::user()->can('edit page') || $is_owner)
															  						<li><a href="{{ route('page.edit', ['id' => $id, 'from' => $key]) }}"><b>{{ $key }}</b> <i class="glyphicon glyphicon-ok"></i></a></li>
															  					@endif	
															  				@else
															  					@if(Auth::user()->hasRole('admin') || Auth::user()->can('translate page'))
															  						<li><a href="{{ route('page.edit', ['id' => $id, 'from' => $default_locale, 'to' => $key]) }}">{{ $key }}</a></li>
															  					@endif	
															  				@endif
															  			@endforeach
															  		@endif
															  	</ul>
															</div>	
			  		  			  		  				@endif
			  		  			  		  				
			  	                    				</td>
		                						</tr>
											@endforeach
        								@else
        									<tr class="odd"><td valign="top" colspan="4" class="dataTables_empty">{{ trans('admin.no_data_in_tbl') }}</td></tr>
        								@endif
        							</tbody>
        						</table>
        					</div>
        				</div>
        			</div>
        		</div>
			</div>
		</div>
	</div>
@endsection