@extends('backpack::layout')

@section('after_scripts')
	{{ Html::script('js/ckeditor/ckeditor.js') }}
	{{ Html::script('js/parsely/parsley.min.js') }}
	{{ Html::script('js/parsely/i18n/en.js') }}
	{{ Html::script('js/admin/page/page_form.js') }}
	{{ Html::script('js/admin/page/edit.js') }}
@endsection

@section('header')
	<section class="content-header">
	  <h1>
	  	{{ trans('admin.edit_page') }}
	  </h1>
	  <ol class="breadcrumb">
	    <li><a href="{{ url($locale.'/admin/dashboard') }}">{{ trans('admin.admin') }}</a></li>
	    <li><a href="{{ route('page.index') }}" class="text-capitalize">{{ trans('admin.pages') }}</a></li>
	    <li class="active">{{ trans('admin.edit') }}</li>
	  </ol>
	</section>
@endsection


@section('content')
	<div class="row">
        <div class="col-md-8 col-md-offset-2">
            <a href="{{ route('page.index') }}"><i class="fa fa-angle-double-left"></i> {{ trans('admin.back_all_pages') }} </a>
            <br><br>
            
            @if (isset($translate_to))
            	<?php 
            		$form_type = $page_types['translate'];
            		$box_title = trans('admin.translate_page');
            	?>
            @else
            	<?php 
            		$form_type = $page_types['edit'];
            		$box_title = trans('admin.edit_page');
            	?>
            @endif
            
            @include('elements.pages.add_edit_form', [
            	'form_data' 	=> ['route' => ['page.update', $page['id']], 'method'=> 'POST', 'id' => 'page_form'], 
            	'form_type' 	=> $form_type, 
            	'box_title' 	=> $box_title
            ])
            
        </div>
    </div>
@endsection


	