@extends('backpack::layout')

@section('after_scripts')
	{{ Html::script('js/ckeditor/ckeditor.js') }}
	{{ Html::script('js/parsely/parsley.min.js') }}
	{{ Html::script('js/parsely/i18n/en.js') }}
	{{ Html::script('js/admin/page/page_form.js') }}
	{{ Html::script('js/admin/page/create.js') }}
@endsection

@section('header')
	<section class="content-header">
	  <h1>
	  	{{ trans('admin.add_page') }}
	  </h1>
	  <ol class="breadcrumb">
	    <li><a href="{{ url($locale.'/admin/dashboard') }}">{{ trans('admin.admin') }}</a></li>
	    <li><a href="{{ route('page.index') }}" class="text-capitalize">{{ trans('admin.pages') }}</a></li>
	    <li class="active">{{ trans('admin.add') }}</li>
	  </ol>
	</section>
@endsection


@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <a href="{{ route('page.index') }}"><i class="fa fa-angle-double-left"></i> {{ trans('admin.back_all_pages') }} </a>
            <br><br>
            
            @include('elements.pages.add_edit_form', [
            	'form_data' => ['route' => ['page.store'], 'method'=> 'POST', 'id' => 'page_form'], 
            	'form_type' => $page_types['add'], 
            	'box_title' => trans('admin.add_page')]
            )
            
        </div>
    </div>
@endsection


	