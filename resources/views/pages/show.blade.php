@extends('layouts.default_layout')

@section('content')

	@include('elements.pages.show_page_header')
	
	@include('elements.pages.show_page_content')
	
@endsection


	