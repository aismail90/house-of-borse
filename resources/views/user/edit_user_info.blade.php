@extends('backpack::layout')
@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <!-- Default box -->


        {!! Form::open(['id'=>'crud_form','model_name'=>'admin/user','route_name'=>'edit_basic_info','url' => $locale.'/admin/edit-user-info']) !!}
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Edit</h3>
            </div>

            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="box-body row">
                <!-- load the view from the application if it exists, otherwise load the one in the package -->

                <!-- load the view from the application if it exists, otherwise load the one in the package -->
                <!-- text input -->
                <div class="form-group col-md-12">
                    <label>Name</label>

                    <input type="text" id="name" name="name" value="{{\Auth::user()->name}}" class="form-control">


                </div>

                <!-- load the view from the application if it exists, otherwise load the one in the package -->
                <!-- text input -->
                <div class="form-group col-md-12">
                    <label>Email</label>
                    <input type="email" name="email" value="{{\Auth::user()->email}}" class="form-control">


                </div>	      <!-- load the view from the application if it exists, otherwise load the one in the package -->
                <!-- password -->
                <div class="form-group col-md-12">
                    <label>Password</label>
                    <input id="password" type="password" name="password" class="form-control">


                </div>	      <!-- load the view from the application if it exists, otherwise load the one in the package -->
                <!-- password -->
                <div class="form-group col-md-12">
                    <label>Password Confirmation</label>
                    <input id="password_confirmation" type="password" name="password_confirmation" class="form-control">


                </div>	      <!-- load the view from the application if it exists, otherwise load the one in the package -->
                <!-- dependencyJson -->


                <!-- load the view from the application if it exists, otherwise load the one in the package -->
                <!-- hidden input -->
                <div class="form-group col-md-12">
                    <input type="hidden" name="id" value="1" class="form-control">
                </div>




            </div><!-- /.box-body -->
            <div class="box-footer">

                <button type="submit" class="btn btn-success ladda-button" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-save"></i> Save</span></button>
                <a href="{{url($locale.'/admin/dashboard')}}" class="btn btn-default ladda-button" data-style="zoom-in"><span class="ladda-label">Cancel</span></a>

            </div><!-- /.box-footer-->
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection

@section('after_scripts')
{{ Html::script('js/parsely/parsley.min.js') }}
{{ Html::script('js/parsely/i18n/en.js') }}
{{ Html::script('js/admin/Backpack/backpack_form_validation.js') }}
@endsection