<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
	<head>
		<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<meta name="description" content="">
		<title>
      		{{ isset($page_title) ? $page_title : trans('portal.website_name') }}
    	</title>
    	
    	@yield('before_styles')
    	
    	<!-- Custom styles for this template -->
    	{{ Html::style('css/style.css') }}
    	{{ Html::style('css/slick.css') }}
    	{{ Html::style('css/animate.css') }}
    	@yield('select_style')
    	<!-- Bootstrap core CSS -->
        {{ Html::style('css/select2.min.css') }}
        {{ Html::style('css/bootstrap.min.css') }}
	    
	    <!-- Bootstrap RTL CSS -->
	    @if(App::getLocale() == 'ar') 
	    	{{ Html::style('css/rtl/bootstrap.css') }}
	   	@endif
	    
	    <!-- Custom styles for this template -->
	    {{ Html::style('css/'.$css_driection.'/app.css') }}
	    
	    {{ Html::script('js/modernizr.js') }} <!-- Modernizr -->
	    	    
	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
			{{ Html::script('js/html5shiv.min.js') }}
			{{ Html::script('js/respond.min.js') }}
	    <![endif]-->
    	
    	@yield('after_styles')
    	
	</head>
	<body class="@if(isset($body_class)) {{ $body_class }} @endif nav-is-fixed @if(App::getLocale() == 'ar') nav-on-left @endif">
		
		@include('elements.header.top_area')
		
		<!-- Main content -->
		<main class="cd-main-content">
			<div class="content">
				<div class="main-content">
					@yield('content')
				</div>
			</div>
			<footer class="footer">
				@include('elements.footer.footer_content')
			</footer>
		</main>
		
		@include('elements.header.menu')
		
		@yield('before_scripts')
		
		{{ Html::script('js/jquery-2.1.1.js') }}
		{{ Html::script('js/bootstrap.min.js') }}
		{{ Html::script('js/ie10-viewport-bug-workaround.js') }}
		{{ Html::script('js/wow.min.js') }}
		{{ Html::script('js/jquery.mobile.custom.min.js') }}
		{{ Html::script('js/main.js') }}
		{{ Html::script('js/slick.min.js') }}		
		{{ Html::script('js/general.js') }}
		
		@yield('after_scripts')
		
	</body>
</html>