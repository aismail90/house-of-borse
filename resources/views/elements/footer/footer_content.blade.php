<section class="container-fluid wow fadeIn animated hidden-xs">
	<div class="row contact">
		<div class="col-md-5 col-sm-4">
			<a href="{{ route('homepage') }}" class="brand">{{ HTML::image('img/houseofborse-logo.svg', trans('portal.website_name')) }}</a>
		</div>
		<div class="col-md-4 contact">
        	<header>
            	<h4>{{ trans('portal.footer_contact_info') }}</h4>
            </header>
            <ul class="list-unstyled">
            	<li>
                	<i class="location"></i>
                	<span>{{ trans('portal.footer_location_part1') }}.<br />
                	{{ trans('portal.footer_location_part2') }}.</span>
              	</li>
              	<li>
                	<i class="mail"></i>
                	<a href="mailto:{{ config('app.mailto_hob') }}">{{ config('app.mailto_hob') }}</a>
              	</li>
              	<li>
                	<i class="phone"></i>
                	<span>{{ trans('portal.footer_hob_phone') }}</span>
              	</li>
            </ul>
        </div>
		<div class="col-md-3 col-sm-4">
        	<header>
            	<h4>{{ trans('portal.footer_follow_us') }}</h4>
            </header>
            <ul class="list-inline social-links">
	            <li>
	            	<a href="{{ config('app.hob_facebook_url') }}" class="facebook" target="_blank"></a>
	            </li>
	            <li>
	            	<a href="{{ config('app.hob_twitter_url') }}" class="twitter" target="_blank"></a>
	            </li>
	            <li>
	            	<a href="{{ config('app.hob_livehelp_url') }}" class="live-chat" target="_blank"></a>
	            </li>
	            <li>
	            	<a href="{{ config('app.hob_youtube_url') }}" class="youtube" target="_blank"></a>
	            </li>
            </ul>
		</div>
	</div>
</section>

@if($menu)
<section class="site-map wow fadeIn animated hidden-xs hidden-sm">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				@foreach($menu as $menu_items)	
				<div>
                	<header>
                  		<h5>{{ trans('portal.'.$menu_items['title']) }}</h5>
                	</header>
                	@if(!empty($menu_items['submenu']))
                	<ul class="list-unstyled">
                		@foreach($menu_items['submenu'] as $submenu_items)
	                  		<li>
	                    		<a href="{{ url($locale.$submenu_items['path']) }}">{{ trans('portal.'.$submenu_items['title']) }}</a>
	                  		</li>
                  		@endforeach
                	</ul>
                	@endif
              	</div>
              	@endforeach
			</div>
		</div>
	</div>
</section>
@endif

<section class="container-fluid risk-warning  wow fadeIn animated">
	<div class="row">
		<div class="col-md-12">
			<article class="hidden-xs">
            	<h6>{{ trans('portal.footer_legal_information') }}:</h6>
              	<p>
                	{{ trans('portal.footer_legal_information_desc') }}
              	</p>
			</article>
            <article class="hidden-xs">
				<h6>{{ trans('portal.footer_risk_warning') }}</h6>
              	<p>
                	{{ trans('portal.footer_risk_warning_desc1') }}
              	</p>
              	<p>
                	{{ trans('portal.footer_risk_warning_desc2') }}
              	</p>
            </article>
            <article class="visible-xs risk-links">
                <ul class="list-inline ">
                  <li>
                    <a href="#">{{ trans('portal.footer_legal_information') }}</a>
                  </li>
                  <li>
                    <a href="#">{{ trans('portal.footer_risk_warning') }}</a>
                  </li>
                </ul>
            </article>
            <article class="copy-right">
              	<p>
                	{{ trans('portal.footer_copyrights') }}
              	</p>
            </article>
		</div>
	</div>
</section>