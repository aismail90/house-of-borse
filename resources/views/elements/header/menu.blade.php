@if($menu)	
	<nav class="cd-nav">
		<ul id="cd-primary-nav" class="nav nav-tabs cd-primary-nav is-fixed">
        	<li>
            	<a class="brand" href="{{ route('homepage') }}">{{ HTML::image('img/houseofborse-logo.svg', trans('portal.website_name')) }}</a>
        	</li>
        	<li>
          		<a href="#" class="my-account"><i></i> {{ trans('portal.header_client_login') }}</a>
        	</li>
        	<?php $i = 0; ?>
        	@foreach($menu as $menu_items)
        		<?php $i++; ?>
        		<li class="has-children @if(array_key_exists('active', $menu_items)) {{ 'active' }} @endif"">
	          		<a href="javascript:void(0);">{{ trans('portal.'.$menu_items['title']) }} @if(count($menu) > $i) <span></span> @endif </a>
	          		@if(!empty($menu_items['submenu']))
	          			<ul class="cd-secondary-nav is-hidden">
	          				<li class="go-back"><a href="#0">{{ trans('portal.back') }}</a></li>
	          				@foreach($menu_items['submenu'] as $submenu_items)
	          					<li @if(array_key_exists('active', $submenu_items)) {{ 'class=active' }} @endif><a href="{{ url($locale.$submenu_items['path']) }}">{{ trans('portal.'.$submenu_items['title']) }}</a></li>
		            		@endforeach
		  				</ul>
	          		@endif
	        	</li>
        	@endforeach
        	<li class="open-account">
          		<div>
            		<a href="#">
              			<i class="live-icon"></i>
              			<h5>
                			<span>{{ trans('portal.home_open') }}</span>
                			{{ trans('portal.home_live_account') }}
              			</h5>
            		</a>
          		</div>
            	<div>
              		<a href="#">
                		<i class="demo-icon"></i>
                		<h5>
                  			<span>{{ trans('portal.home_open') }}</span>
                  			{{ trans('portal.home_demo_account') }}
                		</h5>
              		</a>
            	</div>
        	</li>
		</ul><!-- primary-nav -->
	</nav> <!-- cd-nav -->
@endif