<header class="hidden-nav-sm">
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="{{ route('homepage') }}">{{ HTML::image('img/houseofborse-logo.svg', trans('portal.website_name')) }}</a>
	        </div>
	        <div>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#" class="btn btn-default">{{ trans('portal.header_client_login') }}</a></li>
		            <li><a href="#" class="btn btn-default">{{ trans('portal.header_demo_account') }}</a></li>
		            <li><a href="#" class="btn btn-primary">{{ trans('portal.header_account_opening') }}</a></li>
		            <li class="dropdown lang">
						<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="{{ App::getLocale() }}"></i> <span class="caret"></span></a>
		                <ul class="dropdown-menu">
		                	<?php 
		                		$segments = Request::segments();
		                	?>
		                	@foreach($ddl_languages as $key => $val)
		                		<?php $segments[0] = $key; 
		                			$segments_implode = implode('/', $segments);
		                		?>
		                		<li @if($key == App::getLocale()) class="active" @endif><a href="{{ url($segments_implode) }}"><i class="{{ $key }}"></i>{{ trans('portal.ddl_lang_'.$key) }}</a></li>
		                	@endforeach
		                </ul>
					</li>
				</ul>
			</div>
		</div>
	</nav>
</header>
<header class="cd-main-header navbar-fixed-top" id="header">
	<nav class="main-nav" id="header-container">
		<div class="container-fluid">
			<div class="row">
            	<div class="col-md-12">
              		<a class="navbar-brand" href="{{ route('homepage') }}">{{ HTML::image('img/houseofborse-logo.svg', trans('portal.website_name')) }}</a>
              		<ul class="cd-header-buttons">
                		<li><a class="cd-nav-trigger" href="#cd-primary-nav"><span></span></a></li>
              		</ul> <!-- cd-header-buttons -->
            	</div>
          	</div>
		</div>
	</nav>
</header>