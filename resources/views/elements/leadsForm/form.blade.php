
<form>
    <div class="form-group">
        <div class="has-float-label">
            <input type="text" class="form-control"  placeholder="{{ trans('portal.leads_form_full_name') }}">
            <label>{{ trans('portal.leads_form_full_name') }} </label>
        </div>
    </div>
    <div class="form-group">
        <div class="has-float-label">
            <input type="text" class="form-control" placeholder="{{ trans('portal.leads_form_email_address') }}">
            <label>{{ trans('portal.leads_form_email_address') }}</label>
        </div>

    </div>
    <div class="form-group">

            <select id="countries" class="form-control select2">
                <?php
                $countries=getCountries();
                $currentCountryIso=getCurrentCountry(Request::ip());
                ?>
                @foreach($countries as $country )
                <option code="{{$country->dialing_prefix}}" <?php if($country->ISO3 == $currentCountryIso) {$currentDialingNumber=$country->dialing_prefix; echo('selected');}?>  >

                    @if($locale=='ar')
                    {{$country->name}}
                    @else
                    {{$country->nameenglish}}
                    @endif
                </option>
                @endforeach
            </select>
    </div>

    <div class="form-group">

        <div class="input-group">

            <span class="input-group-addon"><input id="code" class="form-control " type="text" value="{{$currentDialingNumber}}" disabled /></span>

            <div class="has-float-label">
                <input type="text" class="form-control" placeholder="{{ trans('portal.leads_form_phone_number') }}">
                <label>{{ trans('portal.leads_form_phone_number') }}</label>
            </div>
        </div>

    </div>
    <div class="form-group">
        <div class="has-float-label">
            <input type="text" class="form-control" placeholder="{{ trans('portal.leads_form_website') }}">
            <label>{{ trans('portal.leads_form_website') }}</label>
        </div>

    </div>
    <div class="form-group">
        <div class="has-float-label">
            <input type="text" class="form-control" placeholder="{{ trans('portal.leads_form_company_name') }}">
            <label>{{ trans('portal.leads_form_company_name') }}</label>
        </div>

    </div>
    <div class="form-group">
        <div class="has-float-label">
            <input type="text" class="form-control" placeholder="{{ trans('portal.leads_form_subject') }}">
            <label>{{ trans('portal.leads_form_subject') }}</label>
        </div>

    </div>
    <div class="form-group">
        <div class="has-float-label">
            <textarea class="form-control" rows="3" placeholder="{{ trans('portal.leads_form_your_message') }}"></textarea>
            <label>{{ trans('portal.leads_form_your_message') }}</label>
        </div>

    </div>
    <div class="form-group button">
        <button type="submit" class="btn btn-primary">{{ trans('portal.leads_form_send') }}</button>
    </div>

</form>