<div class="side-form">
    <header>
        <span>
            @if($page_slug == 'institutional-services')
            {{ trans('portal.institutional_services_form') }}
            @elseif($page_slug == 'referring-partner')
            {{ trans('portal.referring_partner_form') }}
            @elseif($page_slug == 'white-label')
            {{ trans('portal.white_label_form') }}
            @else
            {{ trans('portal.money_manager_form') }}
            @endif

        </span>
        <h5>
            {{ trans('portal.leads_complete_form') }}
        </h5>
        <span class="arrow"><i></i></span>
    </header>
    @include('elements.leadsForm.form')
</div>