<style>
	.mandatroy { color: red; }
	.parsley-required { color: red; }
</style>

{!! Form::open($form_data) !!}
	<input type="hidden" name="form_type" value="{{ $form_type }}">
	<input type="hidden" name="pair_id" id="pair_id" value="{{ $pair['id'] }}">
	
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">{{ $box_title }}</h3>
		</div>
    	@if($errors->all())
			<div class="col-md-12">
			  	<div class="callout callout-danger">
				    <h4>{{ trans('admin.plz_fix_errors') }}</h4>
				    <ul>
						@foreach($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			</div>
		@endif
		@if(Session::has('message'))
		    <div class="alert alert alert-success">
		      {{Session::get('message')}}
		    </div>
		@endif
		<div class="box-body row">
			<div class="form-group col-md-6">
    			<label>{{ trans('admin.pair') }}</label> <span class="mandatroy">*</span>
    			<?php $pair_form_input = ['placeholder' => trans('admin.pair'), 'class' => 'form-control', 'id' => 'pair'];  ?>
    			@if ($form_type == $page_types['edit'])
					<?php $pair_form_input['disabled'] = TRUE; ?>
				@endif 
                <?php echo Form::text('pair', $pair['pair'], $pair_form_input); ?>
    		</div>
    		<div class="form-group col-md-6">
    			<label>{{ trans('admin.pair_type') }}</label> <span class="mandatroy">*</span>
    			<?php echo Form::select('type', $pair_types,  $pair['type'], ['placeholder' => trans('admin.pair_type'), 'class' => 'form-control', 'id' => 'type']); ?>
      		</div>
    		<div class="form-group col-md-6">
    			<label>{{ trans('admin.min_spread') }}</label>
                <?php echo Form::text('min_spread', $pair['min_spread'], ['placeholder' => trans('admin.min_spread'), 'class' => 'form-control', 'id' => 'min_spread']); ?>
    		</div>
    		<div class="form-group col-md-6">
    			<label>{{ trans('admin.avg_spread') }}</label>
                <?php echo Form::text('avg_spread', $pair['avg_spread'], ['placeholder' => trans('admin.avg_spread'), 'class' => 'form-control', 'id' => 'avg_spread']); ?>
    		</div>
    		
    		
    		<div class="form-group col-md-6">
    			<label>{{ trans('admin.im_factor') }}</label> <span class="mandatroy">*</span>
                <?php echo Form::text('im_factor', $pair['im_factor'], ['placeholder' => trans('admin.im_factor'), 'class' => 'form-control', 'id' => 'im_factor']); ?>
    		</div>
    		<div class="form-group col-md-6">
    			<label>{{ trans('admin.trading_hours') }}</label> <span class="mandatroy">*</span>
                <?php echo Form::text('trading_hours', $pair['trading_hours'], ['placeholder' => trans('admin.trading_hours'), 'class' => 'form-control', 'id' => 'trading_hours']); ?>
    		</div>
    		<div class="form-group col-md-6">
    			<label>{{ trans('admin.value_date') }}</label>
                <?php echo Form::text('value_date', $pair['value_date'], ['placeholder' => trans('admin.value_date'), 'class' => 'form-control', 'id' => 'value_date']); ?>
    		</div>
    		<div class="form-group col-md-6">
    			<label>{{ trans('admin.tick_factor') }}</label> <span class="mandatroy">*</span>
                <?php echo Form::text('tick_factor', $pair['tick_factor'], ['placeholder' => trans('admin.tick_factor'), 'class' => 'form-control', 'id' => 'tick_factor']); ?>
    		</div>
    		<div class="form-group col-md-6">
    			<label>{{ trans('admin.min_trade_size') }}</label>
                <?php echo Form::text('min_trade_size', $pair['min_trade_size'], ['placeholder' => trans('admin.min_trade_size'), 'class' => 'form-control', 'id' => 'min_trade_size']); ?>
    		</div>
    		<div class="form-group col-md-6">
    			<label>{{ trans('admin.contract_size_lot') }}</label>
                <?php echo Form::text('contract_size_lot', $pair['contract_size_lot'], ['placeholder' => trans('admin.contract_size_lot'), 'class' => 'form-control', 'id' => 'contract_size_lot']); ?>
    		</div>
		</div>
		<div class="box-footer">
      		<button type="submit" name="submit" class="btn btn-success ladda-button" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-save"></i> {{ trans('admin.save') }} </span></button>
		</div>
	</div>
{!! Form::close() !!}