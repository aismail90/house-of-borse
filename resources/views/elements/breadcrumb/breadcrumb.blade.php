@if(!empty($breadcrumb))
	<ol class="breadcrumb">
		<li><a href="{{ route('homepage') }}"><i class="glyphicon glyphicon-home"></i></a></li>
		@foreach($breadcrumb as $item)
			<li @if(isset($item['active'])) {{ 'class=active' }} @endif >
				@if(isset($item['path']))
					<a href="{{ url($locale.$item['path']) }}">{{ trans('portal.'.$item['title']) }}</a>
				@else
					<a href="javascript:void(0);">{{ trans('portal.'.$item['title']) }}</a>
				@endif
			</li>
		@endforeach
	</ol>
@endif