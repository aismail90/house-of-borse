<section class="wow fadeIn animated table-pricing">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<article>
					<header>
						<h2>
							<span class="line"></span>
                          	{{ trans('portal.forex_key_benefits_of_trading_forex_through_hob') }}
                        </h2>
					</header>
					<ul class="media-list">
						<li class="media col-md-6">
							<div class="media-left">
                            	<i class="pairs"></i>
                          	</div>
                          	<div class="media-body">
                            	<h4 class="media-heading">{{ trans('portal.forex_wide_variety_of_currency_pairs') }}</h4>
                          	</div>
						</li>
                        <li class="media col-md-6">
                          	<div class="media-left">
                            	<i class="interest"></i>
                          	</div>
                          	<div class="media-body">
                            	<h4 class="media-heading">{{ trans('portal.forex_pure_ecn_brokerage_no_conflict_of_interest') }}</h4>
                          	</div>
                        </li>
                        <li class="media col-md-6">
                          	<div class="media-left">
                            	<i class="banks"></i>
                          	</div>
                          	<div class="media-body">
                            	<h4 class="media-heading">{{ trans('portal.forex_aggregated_liquidity_feeds_from_tier_1_banks') }}</h4>
                          	</div>
                        </li>
                        <li class="media col-md-6">
                          	<div class="media-left">
                            	<i class="spread"></i>
                          	</div>
                          	<div class="media-body">
                            	<h4 class="media-heading">{{ trans('portal.forex_super_tight_spreads_starting_from_0_pips') }}</h4>
                          	</div>
                        </li>
                        <li class="media col-md-6">
                          	<div class="media-left">
                            	<i class="quality"></i>
                          	</div>
                          	<div class="media-body">
                            	<h4 class="media-heading">{{ trans('portal.forex_fast_and_execution_quality_best_available_price') }}.</h4>
                          	</div>
                        </li>
                        <div class="clearfix"></div>
					</ul>
				</article>
				<article>
					<header>
						<h2 class="center">{{ trans('portal.forex_our_business_module_is_designed_for_high_trading_volume') }}</h2>
                    </header>
                      <div class="row">
                        <div class="col-md-4">
                          <div class="pricing silver">
                            <header>
                              <h4>{{ trans('portal.forex_silver_trader') }}</h4>
                            </header>
                            <div class="price-details">
                              <div class="amount">
                                <div class="amount-details">
                                  <div>
                                    <div class="number">1<span>00</span></div>
                                    <div class="text">
                                      <span>{{ trans('portal.forex_less_than') }}</span>
                                      {{ trans('portal.forex_milion') }}
                                    </div>
                                  </div>
                                  <label>{{ trans('portal.forex_traded_per_month') }}</label>
                                </div>
                              </div>
                              <div class="commission">
                                <div class="number">
                                  <span>$</span>45 <label class="lable">{{ trans('portal.forex_commission') }}</label>
                                </div>
                                <label>{{ trans('portal.forex_per_million_traded') }}</label>
                              </div>
                            </div>
                            <footer>
                              <a href="#">{{ trans('portal.forex_join') }} <i></i></a>
                            </footer>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="pricing gold">
                            <header>
                              <h4>{{ trans('portal.forex_gold_trader') }}</h4>
                            </header>
                            <div class="price-details">
                              <div class="amount">
                                <div class="amount-details">
                                  <div>
                                    <div class="number">1</div>
                                    <div class="text">
                                      <span>{{ trans('portal.forex_up_to') }}</span>
                                      {{ trans('portal.forex_yard') }}
                                    </div>
                                  </div>
                                  <label>{{ trans('portal.forex_traded_per_month') }}</label>
                                </div>
                              </div>
                              <div class="commission">
                                <div class="number">
                                  <span>$</span>40 <label class="lable">{{ trans('portal.forex_commission') }}</label>
                                </div>
                                <div class="percentage">
                                  <lable>{{ trans('portal.forex_lower') }}</lable>
                                  <span>11<span>%</span></span>
                                </div>
                              </div>
                            </div>
                            <footer>
                              <a href="#">{{ trans('portal.forex_join') }} <i></i></a>
                            </footer>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="pricing platinum">
                            <header>
                              <h4>{{ trans('portal.forex_platinum_trader') }}</h4>
                            </header>
                            <div class="price-details">
                              <div class="amount">
                                <div class="amount-details">
                                  <div>
                                    <div class="number">1</div>
                                    <div class="text">
                                      <span>{{ trans('portal.forex_more_than') }}</span>
                                      {{ trans('portal.forex_yard') }}
                                    </div>
                                  </div>
                                  <label>{{ trans('portal.forex_traded_per_month') }}</label>
                                </div>
                              </div>
                              <div class="commission">
                                <div class="number">
                                  <span>$</span>35 <label class="lable">{{ trans('portal.forex_commission') }}</label>
                                </div>
                                <div class="percentage">
                                  <lable>{{ trans('portal.forex_lower') }}</lable>
                                  <span>22<span>%</span></span>
                                </div>
                              </div>
                            </div>
                            <footer>
                              <a href="#">{{ trans('portal.forex_join') }}<i></i></a>
                            </footer>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <article class="high-volume">
                            <p>
                             	{{ trans('portal.forex_institutional_and_high_volume_trader') }}
                            </p>
                            <a class="btn btn-primary" href="#">{{ trans('portal.forex_contact_us') }}</a>
                          </article>
                        </div>
                      </div>
				</article>
			</div>
		</div>
	</div>
</section>