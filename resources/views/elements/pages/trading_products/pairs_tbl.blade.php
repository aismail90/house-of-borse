@if($pairs = App('PairManager')->getPairs())

	@section('after_scripts')
		{{ Html::script('js/socket.io-1.4.5.js') }}
		{{ Html::script('js/homepage/homepage.js') }}
	@endsection
		
	@php 
  		$curr_pairs = [];
  	@endphp
	
	<section class="wow fadeIn animated spread-section">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
                    <header>
						<h2 class="center">{{ trans('portal.forex_reduce_your_trading_cost') }}</h2>
                    </header>
                    <ul class="media-list visible-xs visible-sm row ">		
						<li class="media col-xs-6">		
	                    	<div class="media-body">		
	                            {{ trans('portal.forex_best_execution_rates') }}
	                        </div>		
	                        <div class="media-left">		
	                            <a href="#">		
	                              <i class="rates"></i>		
	                            </a>		
	                        </div>		
						</li>		
	                    <li class="media col-xs-6">		
	                    	<div class="media-left">		
	                        	<a href="#">		
	                            	<i class="execution"></i>		
	                            </a>		
	                       	</div>		
	                        <div class="media-body">		
	                            {{ trans('portal.forex_low_execution_time') }}
	                        </div>		
	                    </li>		
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-md-offset-1">
					<div class="tab-main">
						<ul class="nav nav-tabs" role="tablist">
                        	<li class="active"><a href="#common" aria-controls="common" role="tab" data-toggle="tab"><i class="pairs"></i>{{ trans('portal.forex_common_pairs') }}</a></li>
                        	<li><a href="#minors" aria-controls="minors" role="tab" data-toggle="tab">{{ trans('portal.forex_minors') }}</a></li>
                        	<li><a href="#exotic" aria-controls="exotic" role="tab" data-toggle="tab">{{ trans('portal.forex_exotic') }}</a></li>
                      	</ul>
                      	<div class="tab-content">
                      		<div role="tabpanel" class="tab-pane fade in active" id="common">
                      			<table class="table">
                      				<thead>
                      					<tr>
                                 			<th>{{ trans('portal.forex_instrument_pair') }}</th>                                 			
                                 			<th>{{ trans('portal.forex_live') }} <span class="hidden-xs">{{ trans('portal.forex_spread') }}</span></th>
	                                 		<th>{{ trans('portal.forex_minimum') }} <span class="hidden-xs">{{ trans('portal.forex_spread') }}</span></th>
	                                 		<th>{{ trans('portal.forex_average') }} <span class="hidden-xs">{{ trans('portal.forex_spread') }}</span></th>
                               			</tr>
                      				</thead>
                      				<tbody>
                      				@foreach($pairs as $pair)
                      					@if($pair['type'] == 1)
                      						@php
								  				array_push($curr_pairs, $pair['pair']);
								  			@endphp
	                      					<tr>
											    <td>{{ $pair['pair'] }}</td>
											    <td id="spread_change_{{ $pair['pair'] }}"></td>
											    <td>{{ $pair['min_spread'] }}</td>
											    <td>{{ $pair['avg_spread'] }}</td>
											    <td class="hidden"><input type="hidden" id="spread_last2digt_ask_{{ $pair['pair'] }}" value=""></td>
											    <td class="hidden"><input type="hidden" id="spread_last2digt_bid_{{ $pair['pair'] }}" value=""></td>
											</tr>
										@endif
                      				@endforeach
                      				</tbody>
                      			</table>
                      		</div>
                      		<div role="tabpanel" class="tab-pane fade" id="minors">
                      			<table class="table">
                      				<thead>
                      					<tr>
                                 			<th>{{ trans('portal.forex_instrument_pair') }}</th>
                                 			<th>{{ trans('portal.forex_live') }} <span class="hidden-xs">{{ trans('portal.forex_spread') }}</span></th>
	                                 		<th>{{ trans('portal.forex_minimum') }} <span class="hidden-xs">{{ trans('portal.forex_spread') }}</span></th>
	                                 		<th>{{ trans('portal.forex_average') }} <span class="hidden-xs">{{ trans('portal.forex_spread') }}</span></th>
                               			</tr>
                      				</thead>
                      				<tbody>
                      				@foreach($pairs as $pair)
                      					@if($pair['type'] == 2)
                      						@php
								  				array_push($curr_pairs, $pair['pair']);
								  			@endphp
	                      					<tr>
											    <td>{{ $pair['pair'] }}</td>
											    <td id="spread_change_{{ $pair['pair'] }}"></td>
											    <td>{{ $pair['min_spread'] }}</td>
											    <td>{{ $pair['avg_spread'] }}</td>
											    <td class="hidden"><input type="hidden" id="spread_last2digt_ask_{{ $pair['pair'] }}" value=""></td>
											    <td class="hidden"><input type="hidden" id="spread_last2digt_bid_{{ $pair['pair'] }}" value=""></td>
											</tr>
										@endif
                      				@endforeach
                      				</tbody>
                      			</table>
                      		</div>
                        	<div role="tabpanel" class="tab-pane fade" id="exotic">
                        		<table class="table">
                      				<thead>
                      					<tr>
                                 			<th>{{ trans('portal.forex_instrument_pair') }}</th>
                                 			<th>{{ trans('portal.forex_live') }} <span class="hidden-xs">{{ trans('portal.forex_spread') }}</span></th>
	                                 		<th>{{ trans('portal.forex_minimum') }} <span class="hidden-xs">{{ trans('portal.forex_spread') }}</span></th>
	                                 		<th>{{ trans('portal.forex_average') }} <span class="hidden-xs">{{ trans('portal.forex_spread') }}</span></th>
                               			</tr>
                      				</thead>
                      				<tbody>
                      				@foreach($pairs as $pair)
                      					@if($pair['type'] == 3)
                      						@php
								  				array_push($curr_pairs, $pair['pair']);
								  			@endphp
	                      					<tr>
											    <td>{{ $pair['pair'] }}</td>
											    <td id="spread_change_{{ $pair['pair'] }}"></td>
											    <td>{{ $pair['min_spread'] }}</td>
											    <td>{{ $pair['avg_spread'] }}</td>
											    <td class="hidden"><input type="hidden" id="spread_last2digt_ask_{{ $pair['pair'] }}" value=""></td>
											    <td class="hidden"><input type="hidden" id="spread_last2digt_bid_{{ $pair['pair'] }}" value=""></td>
											</tr>
										@endif
                      				@endforeach
                      				</tbody>
                      			</table>
                        	</div>
                        	@php
								$curr_pairs = implode(',', $curr_pairs);
					  		@endphp
					  		<input type="hidden" id="curr_pairs" name="curr_pairs" value="{{ $curr_pairs }}"/>
							<input type="hidden" id="socket_server_url" name="socket_server_url" value="{{ config('app.fx_rates_socket_server_port') }}"/>
                      	</div>
					</div>
				</div>
				<div class="col-md-4 hidden-sm hidden-xs">
					<ul class="media-list">
						<li class="media">
                        	<div class="media-left">
                          		<a href="#">
                            		<i class="rates"></i>
                          		</a>
                        	</div>
                        	<div class="media-body">
                          		{{ trans('portal.forex_best_execution_rates') }}
                        	</div>
                      	</li>
                      	<li class="media">
                        	<div class="media-left">
                        		<a href="#">
                            		<i class="execution"></i>
                          		</a>
                        	</div>
                        	<div class="media-body">
                        		{{ trans('portal.forex_low_execution_time') }}
                        	</div>
                      	</li>
					</ul>
                    <figure>
                    	{{ HTML::image('img/spread.png') }}
                    </figure>
            	</div>
			</div>
		</div>
	</section>
		
@endif