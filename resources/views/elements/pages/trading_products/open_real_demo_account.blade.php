<section class="wow fadeIn animated open-account-footer">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<p>
					{{ trans('portal.forex_get_start_and_try_the_real_market_trade_with_banks') }}
				</p>
				<div class="btn-group">                	
                	<a class="btn btn-primary" href="#"><span class="hidden-xs">{{ trans('portal.forex_open') }}</span> {{ trans('portal.forex_live_account') }} </a>
                	<a class="btn btn-default" href="#"><span class="hidden-xs">{{ trans('portal.forex_open') }}</span> {{ trans('portal.forex_demo_account') }}</a>
                </div>
			</div>
		</div>
	</div>
</section>