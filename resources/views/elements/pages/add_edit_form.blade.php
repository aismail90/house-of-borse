<style>
	.mandatroy { color: red; }
	.parsley-required { color: red; }
</style>

{!! Form::open($form_data) !!}
	<input type="hidden" id="content_css_url" value="{{ URL::asset('css/ltr/app.css') }}">
	<input type="hidden" id="ajaxCreatePageSlugUrl" value="{{ route('page.create_slug') }}">
	<input type="hidden" id="ajaxcheckIfSlugExistsUrl" value="{{ route('page.check_slug') }}">
	<input type="hidden" id="ajaxPagePreviewUrl" value="{{ route('page.preview') }}">
	<input type="hidden" name="form_type" value="{{ $form_type }}">
	<input type="hidden" name="page_id" id="page_id" value="{{ $page['id'] }}">
	
	<input type="hidden" id="ckeditor_path" value="{{ config('app.ckeditor_path') }}">
	<input type="hidden" id="upload_browse_url" value="{{ config('app.ckeditor_upload_browse_url') }}">
	
	@if ($form_type == $page_types['translate'])
		<input type="hidden" name="hdn_page_locale" id="hdn_page_locale" value="{{ $translate_to }}">
	@endif
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">{{ $box_title }}</h3>
			<?php $slug_grp_hdn = ''; ?>
    		@if ($form_type == $page_types['add'])
    			<?php $slug_grp_hdn = 'hidden'; ?>
    		@endif
			<button type="button" id="preview_btn" name="preview" class="btn btn-default ladda-button pull-right preview_page {{ $slug_grp_hdn }}" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-eye"></i> {{ trans('admin.preview') }} </span></button>
		</div>
    	@if($errors->all())
			<div class="col-md-12">
			  	<div class="callout callout-danger">
				    <h4>{{ trans('admin.plz_fix_errors') }}</h4>
				    <ul>
						@foreach($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			</div>
		@endif
		@if(Session::has('message'))
		    <div class="alert alert alert-success">
		      {{Session::get('message')}}
		    </div>
		@endif
		<div class="box-body row">
			@if ($form_type == $page_types['translate'])
				<div class="form-group col-md-6">
	    			<label>{{ trans('admin.trans_from') }}</label>
	    			<?php echo Form::select('page_locale', $page_translated_languages, $page_locale, ['placeholder' => trans('admin.trans_from'), 'class' => 'form-control', 'id' => 'translate_from']); ?>
	      		</div>
			@endif
			<div class="form-group col-md-6">
				@if ($form_type == $page_types['translate'])
					<label>{{ trans('admin.trans_to') }}</label>
					<?php $page_lang = $translate_to; ?>
				@else
					<label>{{ trans('admin.page_language') }}</label> <span class="mandatroy">*</span>
					<?php $page_lang = $page['lang']; ?>	
				@endif
				<?php $languages_form_select = ['class' => 'form-control', 'id' => 'language'];  ?>
				@if ($form_type != $page_types['add'])
					<?php $languages_form_select['placeholder'] = trans('admin.lang'); ?>
				@endif 
				@if ($form_type == $page_types['edit'])
					<?php $languages_form_select['disabled'] = TRUE; ?>
				@endif 
    			<?php echo Form::select('language', $languages, $page_lang, $languages_form_select); ?>
      		</div>
      		@if ($form_type != $page_types['translate'])
      			<div class="form-group col-md-6" style="visibility: hidden;">
	    			<label>hidelang</label>
	    			<?php echo Form::text('hidelang', '', ['class' => 'form-control']); ?>
	      		</div>
      		@endif
      		<div class="form-group col-md-6">
    			<label>{{ trans('admin.parent') }}</label> <span class="mandatroy">*</span>
    			<?php echo Form::select('parent', $parent_pages,  $page['parent'], ['placeholder' => trans('admin.parent'), 'class' => 'form-control', 'id' => 'parent']); ?>
      		</div>
      		<div class="form-group col-md-6">
    			<label>{{ trans('admin.status') }}</label> <span class="mandatroy">*</span>
    			<?php echo Form::select('status', $page_status,  $page['status'], ['placeholder' => trans('admin.status'), 'class' => 'form-control', 'id' => 'status']); ?>
      		</div>
			<div class="form-group col-md-12">
    			<label>{{ trans('admin.title') }}</label> <span class="mandatroy">*</span>
                <?php echo Form::text('title', $page['title'], ['placeholder' => trans('admin.enter_title_here'), 'class' => 'form-control', 'id' => 'title']); ?>
    		</div>
    		<div class="form-group form-group-sm col-md-12 {{ $slug_grp_hdn }}" id="slug_grp">
    			<strong>{{ trans('admin.permalink') }}:</strong>
				@if ($form_type == $page_types['translate'])
					<?php $permalink_locale = $translate_to; ?>
				@else
					<?php $permalink_locale = $page_locale; ?>	
				@endif
				<a href="{{ url('/'.$permalink_locale, [$page['slug']]) }}" id="slug_lnk" class="preview_page">{{ url('/'.$permalink_locale, [$page['slug']]) }}</a>
            	<?php echo Form::text('slug', $page['slug'], ['placeholder' => trans('admin.slug'), 'class' => 'hidden', 'id' => 'slug_input']); ?>
            	<input type="button" value="{{ trans('admin.edit') }}" class="btn btn-default btn-sm" id="slug_btn_edit">
            	<input type="button" value="{{ trans('admin.ok') }}" class="btn btn-default btn-sm hidden" id="slug_btn_ok">
            	<a href="javascript:void(0);" class="hidden" id="slug_btn_cancel">{{ trans('admin.cancel') }}</a>
            	<input type="hidden" id="hdn_slug_val" value="{{ $page['slug'] }}">
            	<input type="hidden" id="hdn_link_val" value="{{ url('/'.$permalink_locale, ['']) }}">
    		</div>
    		<div class="form-group col-md-12">
    			<label>{{ trans('admin.brief') }}</label>
    			<?php echo Form::textarea('brief', $page['brief'], ['id' => 'brief', 'class' => 'form-control', 'rows' => 2]); ?>	
    		</div>
		    <div class="form-group col-md-12">
    			<label>{{ trans('admin.content') }}</label>
    			<?php echo Form::textarea('content', $page['content'], ['id' => 'content', 'class' => 'form-control ckeditor']); ?>	
    		</div>
		</div>
		<div class="box-footer">
      		<button type="submit" name="submit" class="btn btn-success ladda-button" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-save"></i> {{ trans('admin.save') }} </span></button>
<!--       		<button type="submit" name="preview" class="btn btn-default ladda-button" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-eye"></i> {{ trans('admin.preview') }} </span></button> -->
<!--       						<a href="http://localhost:8080/admin/page" class="btn btn-default ladda-button" data-style="zoom-in"><span class="ladda-label">Cancel</span></a> -->
		</div>
	</div>
{!! Form::close() !!}