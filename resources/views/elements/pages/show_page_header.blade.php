<section class="hero">
	<div class="container-fluid">
		<div class="row">
			<article class="col-md-12 @if(!empty($page['brief'])) {{ 'brife' }} @endif wow fadeInUp animated">
				<header>
					 <h1> @if(!empty($page['title']))	{{ $page['title'] }} @endif </h1>
				</header>
				@if(!empty($page['brief']))
					<p>
						{{ $page['brief'] }}
					</p>
				@endif
			</article>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="breadcrumb-main wow fadeIn wow fadeIn animated">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					@include('elements.breadcrumb.breadcrumb')
				</div>
			</div>
		</div>
	</div>
</section>