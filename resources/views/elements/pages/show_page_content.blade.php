@if($page_slug == 'forex')
	<div class="inner-template">
		@php echo checkForElementReplacements($page['content']) @endphp
	</div>
@else
	<section class="container-fluid wow fadeIn animated">
		<div class="row>">
			<article class="@if($page_slug == 'institutional-services'||$page_slug == 'referring-partner'||$page_slug == 'white-label'||$page_slug == 'money-managers') col-md-8  @else col-md-12 @endif inner-template">
				@php echo checkForElementReplacements($page['content']) @endphp
			</article>
			@if($page_slug == 'institutional-services'||$page_slug == 'referring-partner'||$page_slug == 'white-label'||$page_slug == 'money-managers')
				<article class="col-md-4">
                    @section('select_style')
                    {{ Html::style('css/select2.min.css') }}
                    @endsection
                    @section('after_scripts')
                        {{ Html::script('js/select2.full.min.js') }}
                        {{ Html::script('js/leads_forms/form.js') }}
                    @endsection

                    @include('elements.leadsForm.leads')

				</article>
			@endif
		</div>
	</section>
@endif