<section class="hero">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<header class="wow fadeInDown animated">
                   	<h1>{{ trans('portal.home_trading_accounts') }}</h1>
                    <p>
                    	<span>{{ trans('portal.home_trading_accounts_desc1') }}</span> {{ trans('portal.home_trading_account_desc2') }} <span class="hidden-xs">{{ trans('portal.home_trading_account_desc3') }}</span>
                	</p>
				</header>
                <footer class="open-account visible-xs wow fadeInLeft animated">
					<div>
						<a href="#">
                        	<i class="live-icon"></i>
                            <h5>
                            	<span>{{ trans('portal.home_open') }}</span>
                                {{ trans('portal.home_live_account') }}
                           	</h5>
                        </a>
					</div>
                    <div>
						<a href="#">
                        	<i class="demo-icon"></i>
                            <h5>
                                <span>{{ trans('portal.home_open') }}</span>
                            	{{ trans('portal.home_demo_account') }}
                        	</h5>
                    	</a>
                	</div>
				</footer>
				<footer class="btn-group hidden-xs">
                	<a href="#" class="btn btn-default wow fadeInUp animated">{{ trans('portal.home_open') }} {{ trans('portal.home_live_account') }}</a>
                    <a href="#" class="btn btn-default wow fadeInUp animated"> {{ trans('portal.home_open') }} {{ trans('portal.home_demo_account') }}</a>
                </footer>
			</div>
		</div>
	</div>
</section>