<section class="section platforms">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<header class="heading wow fadeIn animated">
					<h2>{{ trans('portal.home_trading_platforms') }}</h2>
                    <span class="hidden-xs"></span>
				</header>
                <div>
                	<div class="tab-content">
                    	<div class="tab-pane fade in active mt4" id="mt4">
                      		<div class="row">
		                        <figure class="col-md-7 col-sm-6 wow fadeInUp animated">
		                        	{{ HTML::image('img/mtr-img.png', trans('portal.home_hob_mt4')) }}
		                        </figure>
	                        	<figcaption class="col-md-5 col-sm-6 wow fadeInRight animated">
	                          		<header>
	                            		<h3>
	                              			<i></i>
	                              			<span>{{ trans('portal.home_trading_platforms1') }} <span class="hidden-xxs">({{ trans('portal.home_mt4') }})</span></span>
	                            		</h3>
	                          		</header>
	                          		<p>{{ trans('portal.home_trading_platforms1_desc') }}</p>
	                          		<footer>
	                            		<a href="{{ route('page.show', ['page_slug' => 'metatrader-4-spot-platform']) }}" class="btn btn-primary">{{ trans('portal.home_try_it') }}</a>
	                          		</footer>
	                        	</figcaption>
                      		</div>
                    	</div>
                  	</div>
				</div>
			</div>
		</div>
	</div>
</section>