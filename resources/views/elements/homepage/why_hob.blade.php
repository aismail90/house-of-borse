<section class="section">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<header class="heading wow fadeIn animated">
                	<h2>{{ trans('portal.home_why_hob') }}</h2>
                    <p class="hidden-xs hidden-sm">{{ trans('portal.home_why_hob_desc') }}</p>
				</header>
				<div class="row">
					<div class="col-md-10 col-md-offset-1 wow fadeInUp animated">
						<ul class="list-unstyled why-hob">
                        	<li class="col-md-6">
                          		<div class="media">
                            		<div class="media-left">
                                		<i class="bank"></i>
                            		</div>
                            		<div class="media-body">
                              			<h4 class="media-heading">{{ trans('portal.home_reason1_to_trade') }}</h4>
                            		</div>
                          		</div>
                        	</li>
                        	<li class="col-md-6">
                          		<div class="media">
                            		<div class="media-left">
                                		<i class="fca"></i>
                            		</div>
                            		<div class="media-body">
                              			<h4 class="media-heading">{{ trans('portal.home_reason2_to_trade') }}</h4>
                            		</div>
                          		</div>
                        	</li>
                        	<li class="col-md-6">
                          		<div class="media">
                            		<div class="media-left">
                                		<i class="spreads"></i>
                            		</div>
                            		<div class="media-body">
                              			<h4 class="media-heading">{{ trans('portal.home_reason3_to_trade') }}</h4>
                            		</div>
                          		</div>
                        	</li>
                        	<li class="col-md-6">
                          		<div class="media">
                            		<div class="media-left">
                                		<i class="seg-fund"></i>
                            		</div>
                            		<div class="media-body">
                              			<h4 class="media-heading">{{ trans('portal.home_reason4_to_trade') }}</h4>
                            		</div>
                          		</div>
                        	</li>
                        	<li class="col-md-6">
                          		<div class="media">
                            		<div class="media-left">
                                		<i class="latency-exec"></i>
                            		</div>
                            		<div class="media-body">
                              			<h4 class="media-heading">{{ trans('portal.home_reason5_to_trade') }}</h4>
                            		</div>
                          		</div>
                        	</li>
                        	<li class="col-md-6">
                          		<div class="media">
                            		<div class="media-left">
                                		<i class="markups"></i>
                            		</div>
                            		<div class="media-body">
                              			<h4 class="media-heading">{{ trans('portal.home_reason6_to_trade') }}</h4>
                            		</div>
                          		</div>
                        	</li>
                        	<li class="col-md-6">
                          		<div class="media">
                            		<div class="media-left">
                                		<i class="deal-desk"></i>
                            		</div>
                            		<div class="media-body">
                              			<h4 class="media-heading">{{ trans('portal.home_reason7_to_trade') }}</h4>
                            		</div>
                          		</div>
                        	</li>
                        	<li class="col-md-6">
                          		<div class="media">
                            		<div class="media-left">
                                		<i class="platforms"></i>
                            		</div>
                            		<div class="media-body">
                              			<h4 class="media-heading">{{ trans('portal.home_reason8_to_trade') }}</h4>
                            		</div>
                          		</div>
                        	</li>
                      	</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>