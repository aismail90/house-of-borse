@if(isset($spread))
	<input type="hidden" id="curr_pairs" name="curr_pairs" value="{{ $curr_pairs }}"/>
	<input type="hidden" id="socket_server_url" name="socket_server_url" value="{{ config('app.fx_rates_socket_server_port') }}"/>
	<section class="spread-block">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="responsive-slide wow fadeInUp animated">
						@foreach($spread as $key => $value)
	                		<?php 
	                			$value = $value[0];
	                			
	                			$check_precision = 5;
	                			if (strpos($key, 'JPY') !== false) {
	                				$check_precision = 3;
	                			}
	                			
	                			$ask = $value['ask'];
	                			$bid = $value['bid'];
	                			
	                			$ask = addZeroes($value['ask'], $check_precision);
	                			$bid = addZeroes($value['bid'], $check_precision);
	                			
	                			$last2digt_ask = substr($ask, -2);
	                			$last2digt_bid = substr($bid, -2);
	                			$change = abs($last2digt_ask - $last2digt_bid) /10;
	                		?>
	                		<div class="parent">
		                    	<div class="item">
									<span>{{ trans('portal.home_spread') }}</span>
		                            <div class="spread-value">
										@if($value['direction'] == 1)
		                					<i class="arrow up" id="spread_arrow_{{ $key }}"></i>
		                				@elseif($value['direction'] == -1)
		                					<i class="arrow down" id="spread_arrow_{{ $key }}"></i>
		                				@else
		                					<i class="arrow no-change" id="spread_arrow_{{ $key }}"></i>	
		                				@endif
		                				<span id="spread_change_{{ $key }}">{{ $change }}</span>
		                            </div>
		                            <div class="spread-info">
										<span>{{ addSlashToCurrency($key) }}</span>
		                              	<span class="hidden-xxs"><ask id="spread_ask_{{ $key }}">{{ $ask }}</ask>/<bid id="spread_bid_{{ $key }}">{{ $last2digt_bid }}</bid></span>
		                              	<span class="visible-xxs">{{ trans('portal.home_spread') }}</span>
		                            </div>
								</div>
								<input type="hidden" id="spread_last2digt_ask_{{ $key }}" value="{{ $last2digt_ask }}">
	                			<input type="hidden" id="spread_last2digt_bid_{{ $key }}" value="{{ $last2digt_bid }}">
		                    </div>
	                	@endforeach
					</div>
				</div>
			</div>
		</div>
	</section>
@endif