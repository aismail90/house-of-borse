<section class="section products">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<header class="heading wow fadeIn animated">
					<h2>{{ trans('portal.home_our_products') }}</h2>
                    <span></span>
				</header>
				<ul class="list-unstyled wow fadeInUp animated">
					<li class="col-md-4 col-sm-6">
						<div class="media">
                        	<div class="media-left">
                          		<i class="forex"></i>
                        	</div>
                        	<div class="media-body">
                          		<h4 class="media-heading">{{ trans('portal.home_our_porducts1') }}</h4>
                          		<p>{{ trans('portal.home_our_porducts1_desc') }}</p>
                          		<a href="{{ route('page.show', ['page_slug' => 'forex']) }}">{{ trans('portal.home_read_more') }}</a>
                        	</div>
                      	</div>
					</li>
					<li class="col-md-4 col-sm-6">
						<div class="media">
                        	<div class="media-left">
                          		<i class="metals"></i>
                        	</div>
                        	<div class="media-body">
                          		<h4 class="media-heading">{{ trans('portal.home_our_porducts2') }}</h4>
                          		<p>{{ trans('portal.home_our_porducts2_desc') }}</p>
                          		<a href="{{ route('page.show', ['page_slug' => 'metals']) }}">{{ trans('portal.home_read_more') }}</a>
                        	</div>
                      	</div>
					</li>
					<li class="col-md-4 col-sm-6">
						<div class="media">
                        	<div class="media-left">
                          		<i class="options"></i>
                        	</div>
                        	<div class="media-body">
                          		<h4 class="media-heading">{{ trans('portal.home_our_porducts3') }}</h4>
                          		<p></p>
                          		<a href="{{ route('page.show', ['page_slug' => 'stock-options']) }}">{{ trans('portal.home_read_more') }}</a>
                        	</div>
                      	</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>