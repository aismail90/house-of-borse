<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::GET('/', ['as' => 'homepage', 'uses' => 'HomePageController@index']);

Route::group(['prefix' => 'admin', 'middleware' => ['backend']], function()
{
	Route::GET('pages', ['as' => 'page.index', 'uses' => 'Admin\PageController@index']);

    Route::GET('page-create', ['as' => 'page.create', 'uses' => 'Admin\PageController@create']);

    Route::POST('page', ['as' => 'page.store', 'uses' => 'Admin\PageController@store']);

	Route::GET('page-edit/{id}', ['as' => 'page.edit', 'uses' => 'Admin\PageController@edit']);

    Route::POST('page/{id}', ['as' => 'page.update', 'uses' => 'Admin\PageController@update']);

    Route::GET('page-create-slug', ['as' => 'page.create_slug', 'uses' => 'Admin\PageController@ajaxCreatePageSlug']);

    Route::GET('page-check-slug', ['as' => 'page.check_slug', 'uses' => 'Admin\PageController@ajaxCheckIfSlugExists']);

    Route::POST('page-preview', ['as' => 'page.preview', 'uses' => 'Admin\PageController@ajaxCreatePreviewPage']);

    Route::GET('dashboard', 'Admin\Backpack\AdminController@dashboard');

	Route::get('logout', 'Auth\LoginController@logout');
	
    Route::GET('edit-user-info', 'Admin\Backpack\PermissionManager\UserCrudController@showBasicInfoForm')->name('editBasicInfo');
    
    Route::POST('edit-user-info', 'Admin\Backpack\PermissionManager\UserCrudController@editBasicInfo');
    
    Route::GET('pairs', ['as' => 'pair.index', 'uses' => 'Admin\PairController@index']);
    
	Route::GET('pair-create', ['as' => 'pair.create', 'uses' => 'Admin\PairController@create']);
	
	Route::POST('pair', ['as' => 'pair.store', 'uses' => 'Admin\PairController@store']);
	
	Route::GET('pair-edit/{id}', ['as' => 'pair.edit', 'uses' => 'Admin\PairController@edit']);
	
	Route::POST('pair/{id}', ['as' => 'pair.update', 'uses' => 'Admin\PairController@update']);
	
	Route::GET('quick-update-pair', ['as' => 'pair.quick_update', 'uses' => 'Admin\PairController@ajaxQuickUpdatePair']);
        
    Route::GET('/','Admin\Backpack\AdminController@redirect');

});
	
Route::group(['prefix' => 'admin'], function() {
	$this->GET('login', 'Auth\LoginController@showLoginForm')->name('login');
    $this->POST('login', 'Auth\LoginController@login');

        // Registration Routes...
//         $this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
//         $this->post('register', 'Auth\RegisterController@register');

        // Password Reset Routes...
    $this->GET('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
    $this->POST('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    $this->GET('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
	$this->POST('password/reset', 'Auth\ResetPasswordController@reset');
});

Route::group(['namespace' => 'Admin\Backpack\PermissionManager'], function () {
	Route::group(['prefix' => config('backpack.base.route_prefix','admin'), 'middleware' => ['web', 'backend']], function () {
    	\CRUD::resource('user', 'UserCrudController');
        \CRUD::resource('role', 'RoleCrudController');
        \CRUD::resource('permission', 'PermissionCrudController');
    });
});
	

Route::group(['namespace' => 'Admin\Backpack\LangFileManager'], function ($router) {
	Route::group(['prefix' => 'admin', 'middleware' => ['web', 'backend']], function () {
    	// Language
		Route::get('language/texts/{lang?}/{file?}', 'LanguageCrudController@showTexts');
        Route::post('language/texts/{lang}/{file}', 'LanguageCrudController@updateTexts');
        Route::resource('language', 'LanguageCrudController');
    });
});
    
Route::GET('{page_slug}/{preview?}', ['as' => 'page.show', 	'uses' => 'Admin\PageController@show'] );
